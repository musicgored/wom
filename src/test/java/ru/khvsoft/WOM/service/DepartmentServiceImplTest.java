package ru.khvsoft.WOM.service;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import ru.khvsoft.WOM.entity.Department;
import ru.khvsoft.WOM.repo.DepartmentRepo;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class DepartmentServiceImplTest {

    @MockBean
    DepartmentRepo departmentRepo;

    @Autowired
    DepartmentService departmentService;

    @Test
    void addDepartment() {
        Department department = new Department("dfdfdf");
        Department departmentWithId = new Department(1,"dfdfdf");
        Mockito.doReturn(departmentWithId).when(departmentRepo).save(department);
        assertEquals(departmentService.addDepartment(department),departmentWithId);
        Mockito.verify(departmentRepo,Mockito.times(1)).save(department);
    }

    @Test
    void getDepartment() {
        Department department = new Department("ddfdf");
        Mockito.doReturn(List.of(department)).when(departmentRepo).findAll();
        assertFalse(departmentService.getDepartment().isEmpty());
    }
}