package ru.khvsoft.WOM.service;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import ru.khvsoft.WOM.entity.Contract;
import ru.khvsoft.WOM.entity.ContractWork;
import ru.khvsoft.WOM.entity.Employee;
import ru.khvsoft.WOM.repo.EmployeeRepo;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class EmployeeServiceImplTest {

    @MockBean
    EmployeeRepo employeeRepo;

    @Autowired
    EmployeeService employeeService;

    @Test
    void getAllEmployee() {
        Mockito.doReturn(List.of(new Employee("323","fefef","fefefefe"))).when(employeeRepo).findAll();
        assertFalse(employeeService.getAllEmployee().isEmpty());
        Mockito.verify(employeeRepo,Mockito.times(1)).findAll();
    }

    @Test
    void addEmployee() {
        Employee employee = new Employee("323","fefef","fefefefe");
        Employee employeeWithId = new Employee(1,"323","fefef","fefefefe");
        Mockito.doReturn(employeeWithId).when(employeeRepo).save(employee);
        assertEquals(employeeWithId,employeeService.addEmployee(employee));
        Mockito.verify(employeeRepo,Mockito.times(1)).save(employee);
    }

    @Test
    void getEmployee() {
        Employee employee = new Employee(1,"323","fefef","fefefefe");
        Optional<Employee> employeeOptional = Optional.of(employee);
        Mockito.doReturn(employeeOptional).when(employeeRepo).findById(1L);
        assertEquals(employee,employeeService.getEmployee(1L).get());
        Mockito.verify(employeeRepo,Mockito.times(1)).findById(1L);
    }

    @Test
    void deleteEmployee() {
        Employee employee = new Employee(1,"323","fefef","fefefefe");
        employeeService.deleteEmployee(1L);
        Mockito.verify(employeeRepo,Mockito.times(1)).deleteById(1L);
    }

//    @Test
//    void selectContactWorkWhereEmployeeId(){
//        Employee employee = new Employee(1,"Evgeny","Vitalievich","Khapuzhenkov");
//        Contract contract = new Contract(2,"work_of_nzmp", LocalDate.of(2020,10,20), LocalDate.of(2021,02,11));
//        ContractWork contractWork = new ContractWork(LocalDate.of(2020,10,9),employee,contract);
//        contractWork.setId(3);
//        Optional<Employee> employeeOptional = Optional.of(employee);
//        Mockito.doReturn(employeeOptional).when(employeeRepo).selectContactWorkWhereEmployeeId(employee.getId());
//        assertEquals(employeeOptional,employeeService.selectContactWorkWhereEmployeeId(employee.getId()));
//        Mockito.verify(employeeRepo,Mockito.times(1)).selectContactWorkWhereEmployeeId(employee.getId());
//    }
}