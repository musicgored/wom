package ru.khvsoft.WOM.service;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import ru.khvsoft.WOM.entity.Contract;
import ru.khvsoft.WOM.repo.ContractRepo;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ContractServiceImplTest {

    @Autowired
    ContractService contractService;

    @MockBean
    ContractRepo contractRepo;

    @Test
    void addContract() {
        Contract contract = new Contract("jkjdjd",LocalDate.of(2022,10,22),LocalDate.of(2000,10,23));
        Contract contractWithId = new Contract(1,"jkjdjd",LocalDate.of(2022,10,22),LocalDate.of(2000,10,23));
        Mockito.doReturn(contractWithId).when(contractRepo).save(contract);
        assertEquals(contractWithId,contractService.addContract(contract));
        Mockito.verify(contractRepo,Mockito.times(1)).save(contract);
    }

    @Test
    void findContract() {
        Contract contractWithId = new Contract(1,"jkjdjd",LocalDate.of(2022,10,22),LocalDate.of(2000,10,23));
        Optional<Contract> contractOptional = Optional.of(contractWithId);
        Mockito.doReturn(contractOptional).when(contractRepo).findById(1L);
        assertEquals(contractOptional,contractService.findContract(1L));
        Mockito.verify(contractRepo,Mockito.times(1)).findById(1L);
    }

    @Test
    void deleteContract() {
        contractService.deleteContract(1L);
        Mockito.verify(contractRepo,Mockito.times(1)).deleteById(1L);
    }

    @Test
    void findAllContract() {
        Contract contractWithId = new Contract(1,"jkjdjd",LocalDate.of(2022,10,22),LocalDate.of(2000,10,23));
        List<Contract> contractList = List.of(contractWithId);
        Mockito.doReturn(contractList).when(contractRepo).findAll();
        assertFalse(contractService.findAllContract().isEmpty());
        Mockito.verify(contractRepo, Mockito.times(1)).findAll();
    }
}