package ru.khvsoft.WOM.service;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import ru.khvsoft.WOM.classes.ReasonSkip;
import ru.khvsoft.WOM.entity.Employee;
import ru.khvsoft.WOM.entity.SkipWork;
import ru.khvsoft.WOM.repo.SkipWorkRepo;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class SkipWorkServiceImplTest {

    @Autowired
    SkipWorkService skipWorkService;

    @MockBean
    SkipWorkRepo skipWorkRepo;

    @Test
    void addSkipWork() {
        SkipWork skipWork = new SkipWork(ReasonSkip.VACATION, LocalDate.of(2022,10,22),LocalDate.of(2020,10,23));
        SkipWork skipWorkWithId = new SkipWork(1,ReasonSkip.VACATION, LocalDate.of(2022,10,22),LocalDate.of(2020,10,23));
        Mockito.doReturn(skipWorkWithId).when(skipWorkRepo).save(skipWork);
        assertEquals(skipWorkService.addSkipWork(skipWork),skipWorkWithId);
        Mockito.verify(skipWorkRepo,Mockito.times(1)).save(skipWork);
    }

    @Test
    void finAllSkipWork() {
        SkipWork skipWorkWithId = new SkipWork(1,ReasonSkip.VACATION, LocalDate.of(2022,10,22),LocalDate.of(2020,10,23));
        List<SkipWork> skipWorkList = List.of(skipWorkWithId);
        Mockito.doReturn(skipWorkList).when(skipWorkRepo).findAll();
        assertEquals(skipWorkList,skipWorkService.finAllSkipWork());
        Mockito.verify(skipWorkRepo,Mockito.times(1)).findAll();
    }

    @Test
    void findSkipWorkById() {
        SkipWork skipWorkWithId = new SkipWork(1,ReasonSkip.VACATION, LocalDate.of(2022,10,22),LocalDate.of(2020,10,23));
        Optional<SkipWork> skipWorkOptional = Optional.of(skipWorkWithId);
        Mockito.doReturn(skipWorkOptional).when(skipWorkRepo).findById(1L);
        assertEquals(skipWorkOptional,skipWorkService.findSkipWorkById(1L));
        Mockito.verify(skipWorkRepo,Mockito.times(1)).findById(1L);
    }

    @Test
    void deleteSkipWorkById() {
        skipWorkService.deleteSkipWorkById(1L);
        Mockito.verify(skipWorkRepo,Mockito.times(1)).deleteById(1L);
    }

    @Test
    void selectSkipWorkWhereReasonSkip(){
        SkipWork skipWorkWithId = new SkipWork(1,ReasonSkip.VACATION, LocalDate.of(2022,10,22),LocalDate.of(2020,10,23));
        List<SkipWork> skipWorkList = List.of(skipWorkWithId);
        Mockito.doReturn(skipWorkList).when(skipWorkRepo).selectSkipWorkWhereReasonSkip(ReasonSkip.VACATION);
        assertEquals(1,skipWorkService.selectSkipWorkWhereReasonSkip(ReasonSkip.VACATION).size());
        Mockito.verify(skipWorkRepo, Mockito.times(1)).selectSkipWorkWhereReasonSkip(ReasonSkip.VACATION);
    }

    @Test
    void selectSkipWorkWhereEmployeeIdAndReasonSkip(){
        Employee employee = new Employee("sdfsdf","sdfsdf","dasdasdasd");
        SkipWork skipWork = new SkipWork(ReasonSkip.MEDICAL, employee,LocalDate.of(2021,01,02),LocalDate.of(2022,02,11));
        List<SkipWork> skipWorkList = List.of(skipWork);
        Mockito.doReturn(skipWorkList).when(skipWorkRepo).selectSkipWorkWhereEmployeeIdAndReasonSkip(employee.getId(),skipWork.getReasonSkip());
        assertEquals(1,skipWorkService.selectSkipWorkWhereEmployeeIdAndReasonSkip(employee.getId(),skipWork.getReasonSkip()).size());
        Mockito.verify(skipWorkRepo,Mockito.times(1)).selectSkipWorkWhereEmployeeIdAndReasonSkip(employee.getId(),skipWork.getReasonSkip());
    }
}