package ru.khvsoft.WOM.service;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import ru.khvsoft.WOM.entity.Contract;
import ru.khvsoft.WOM.entity.ContractWork;
import ru.khvsoft.WOM.entity.Employee;
import ru.khvsoft.WOM.repo.ContractWorkRepo;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ContractWorkServiceImplTest {

    @Autowired
    ContractWorkService contractWorkService;

    @MockBean
    ContractWorkRepo contractWorkRepo;

    @Test
    void addContractWork() {
        ContractWork contractWork = new ContractWork(LocalDate.of(2020,05,22));
        ContractWork contractWorkWithId = new ContractWork(1,LocalDate.of(2020,05,22));
        Mockito.doReturn(contractWorkWithId).when(contractWorkRepo).save(contractWork);
        assertEquals(contractWorkWithId,contractWorkService.addContractWork(contractWork));
        Mockito.verify(contractWorkRepo,Mockito.times(1)).save(contractWork);
    }

    @Test
    void findContractWorkById() {
        ContractWork contractWorkWithId = new ContractWork(1,LocalDate.of(2020,05,22));
        Optional<ContractWork> contractWorkOptional = Optional.of(contractWorkWithId);
        Mockito.doReturn(contractWorkOptional).when(contractWorkRepo).findById(1L);
        assertEquals(contractWorkOptional,contractWorkService.findContractWorkById(1L));
        Mockito.verify(contractWorkRepo,Mockito.times(1)).findById(1L);
    }

    @Test
    void findContractWorkAll() {
        ContractWork contractWorkWithId = new ContractWork(1,LocalDate.of(2020,05,22));
        List<ContractWork> contractWorkList = List.of(contractWorkWithId);
        Mockito.doReturn(contractWorkList).when(contractWorkRepo).findAll();
        assertFalse(contractWorkRepo.findAll().isEmpty());
        Mockito.verify(contractWorkRepo,Mockito.times(1)).findAll();
    }

    @Test
    void deleteContractWorkById() {
        contractWorkService.deleteContractWorkById(1L);
        Mockito.verify(contractWorkRepo,Mockito.times(1)).deleteById(1L);
    }

    @Test
    void selectContactWorkWhereEmployeeId(){
        Employee employee = new Employee("Evgeny","Vitalievich","Khapuzhenkov");
        Contract contract = new Contract("work_of_nzmp", LocalDate.of(2020,10,20), LocalDate.of(2021,02,11));
        ContractWork contractWork = new ContractWork(LocalDate.of(2020,10,9),employee,contract);
        List<ContractWork> contractWorkList = List.of(contractWork);
        Mockito.doReturn(contractWorkList).when(contractWorkRepo).selectContactWorkWhereEmployeeId(employee.getId());
        assertEquals(contractWorkList.size(),contractWorkService.selectContactWorkWhereEmployeeId(employee.getId()).size());
        Mockito.verify(contractWorkRepo,Mockito.times(1)).selectContactWorkWhereEmployeeId(employee.getId());
    }

    @Test
    void selectContractWorkWhereEmployeeIdAndDateWork(){
        Employee employee = new Employee("Evgeny","Vitalievich","Khapuzhenkov");
        Contract contract = new Contract("work_of_nzmp", LocalDate.of(2020,10,20), LocalDate.of(2021,02,11));
        ContractWork contractWork = new ContractWork(LocalDate.of(2020,10,9),employee,contract);
        List<ContractWork> contractWorkList = List.of(contractWork);
        Mockito.doReturn(contractWorkList).when(contractWorkRepo).selectContractWorkWhereEmployeeIdAndDateWork(employee.getId(),contractWork.getDateWork());
        assertEquals(1,contractWorkService.selectContractWorkWhereEmployeeIdAndDateWork(employee.getId(),contractWork.getDateWork()).size());
        Mockito.verify(contractWorkRepo,Mockito.times(1)).selectContractWorkWhereEmployeeIdAndDateWork(employee.getId(),contractWork.getDateWork());
    }

    @Test
    void selectContractWorkBetweenStartDateAndEndDate(){
        Employee employee = new Employee("Evgeny","Vitalievich","Khapuzhenkov");
        Contract contract = new Contract("work_of_nzmp", LocalDate.of(2020,10,20), LocalDate.of(2021,02,11));
        ContractWork contractWork = new ContractWork(LocalDate.of(2020,10,9),employee,contract);
        List<ContractWork> contractWorkList = List.of(contractWork);
        LocalDate startDate = LocalDate.of(2020,10,8);
        LocalDate endDate = LocalDate.of(2020,10,12);
        Mockito.doReturn(contractWorkList).when(contractWorkRepo).selectContractWorkBetweenStartDateAndEndDate(startDate,endDate);
        assertEquals(1,contractWorkService.selectContractWorkBetweenStartDateAndEndDate(startDate,endDate).size());
        Mockito.verify(contractWorkRepo,Mockito.times(1)).selectContractWorkBetweenStartDateAndEndDate(startDate,endDate);
    }
}