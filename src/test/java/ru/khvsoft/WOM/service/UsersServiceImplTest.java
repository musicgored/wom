package ru.khvsoft.WOM.service;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import ru.khvsoft.WOM.entity.Users;
import ru.khvsoft.WOM.repo.UserRepo;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class UsersServiceImplTest {

    @MockBean
    UserRepo userRepo;

    @Autowired
    UsersService usersService;

    @Test
    void addUser() {
    }

    @Test
    void isCorrectPassword() {
        Users user = new Users();
        String arrayUserPasswords[] = {"111", " 23123","   ","2asd", "       ","777777"};
        for (String password:arrayUserPasswords) {
            user.setPassword(password);
            assertFalse(usersService.isCorrectUserName(user));
        }
    }

    @Test
    void isCorrectUserName() {
        Users user = new Users();
        String arrayUserNames[] = {"as", " asss","  ","ass "};
        for (String userName:arrayUserNames) {
            user.setName(userName);
            assertFalse(usersService.isCorrectUserName(user));
        }
    }

    @Test
    void existUserInDatabase() {
        Users user = new Users();
        user.setName("svoloch");
        Mockito.doReturn(List.of(user)).when(userRepo).existUserInDatabase("svoloch");
        assertFalse(usersService.existUserInDatabase(user));
        Mockito.verify(userRepo,Mockito.times(1)).existUserInDatabase("svoloch");
    }

    @Test
    void isCorrectEmail() {
        Users user = new Users();
        String arrayEmails[] = {"asdsd", "asssas@","asssas.ru","ass sas@mail.ru"};
        for (String email:arrayEmails) {
            user.setEmail(email);
            assertFalse(usersService.isCorrectUserName(user));
        }
    }
}