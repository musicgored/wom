package ru.khvsoft.WOM.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CheckServiceImplTest {

    @Autowired
    CheckService checkService;

    @Test
    void checkDate() {
        String[] testDateArrayPass = {"2020-12-30","1999-12-12"};
        String[] testDateArrayError = {"202-12-30","1999-13-12","2000-12-32","3000-12-22","1-12-30"};

        for (String stringDate:testDateArrayPass) {
            assertTrue(checkService.checkDate(stringDate));
        }
        for (String stringDate:testDateArrayError) {
            assertFalse(checkService.checkDate(stringDate));
        }

    }
}