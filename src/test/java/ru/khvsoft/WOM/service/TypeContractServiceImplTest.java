package ru.khvsoft.WOM.service;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import ru.khvsoft.WOM.entity.TypeContract;
import ru.khvsoft.WOM.repo.TypeContractRepo;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class TypeContractServiceImplTest {

    @Autowired
    TypeContractService typeContractService;

    @MockBean
    TypeContractRepo typeContractRepo;

    @Test
    void addTypeContract() {
        TypeContract typeContract = new TypeContract("djkdjd", LocalDate.of(2022,02,01));
        TypeContract typeContractWithId = new TypeContract(1,"djkdjd", LocalDate.of(2022,02,01));
        Mockito.doReturn(typeContractWithId).when(typeContractRepo).save(typeContract);
        assertEquals(typeContractWithId,typeContractService.addTypeContract(typeContract));
        Mockito.verify(typeContractRepo,Mockito.times(1)).save(typeContract);
    }

    @Test
    void findAllTypeContract() {
        TypeContract typeContractWithId = new TypeContract(1,"djkdjd", LocalDate.of(2022,02,01));
        Mockito.doReturn(List.of(typeContractWithId)).when(typeContractRepo).findAll();
        assertFalse(typeContractService.findAllTypeContract().isEmpty());
        Mockito.verify(typeContractRepo, Mockito.times(1)).findAll();
    }

    @Test
    void findByIdTypeContract() {
        TypeContract typeContractWithId = new TypeContract(1,"djkdjd", LocalDate.of(2022,02,01));
        Optional<TypeContract> typeContractOptional = Optional.of(typeContractWithId);
        Mockito.doReturn(typeContractOptional).when(typeContractRepo).findById(1L);
        assertEquals(typeContractOptional,typeContractService.findByIdTypeContract(1L));
        Mockito.verify(typeContractRepo,Mockito.times(1)).findById(1L);
    }

    @Test
    void deleteTypeContractById() {
        typeContractService.deleteTypeContractById(1L);
        Mockito.verify(typeContractRepo,Mockito.times(1)).deleteById(1L);
    }
}