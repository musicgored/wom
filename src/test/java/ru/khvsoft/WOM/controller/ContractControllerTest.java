//package ru.khvsoft.WOM.controller;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import org.junit.jupiter.api.Test;
//import org.mockito.Mockito;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.http.MediaType;
//import org.springframework.security.test.context.support.WithMockUser;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.ResultMatcher;
//import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
//import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
//import ru.khvsoft.WOM.entity.Contract;
//import ru.khvsoft.WOM.service.ContractService;
//
//import java.time.LocalDate;
//import java.util.List;
//import java.util.Optional;
//
//import static org.hamcrest.Matchers.*;
//import static org.junit.jupiter.api.Assertions.*;
//import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
//import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
//import static org.springframework.test.web.client.match.MockRestRequestMatchers.jsonPath;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//@SpringBootTest
//@AutoConfigureMockMvc
//class ContractControllerTest {
//
//    @MockBean
//    ContractService contractService;
//
//    @Autowired
//    MockMvc mockMvc;
//
//    @Autowired
//    private ObjectMapper objectMapper;
//
//    @WithMockUser(authorities = { "ROLE_USER" })
//    @Test
//    void getContracts() throws Exception {
//        Contract contract = new Contract("jkjdjd", LocalDate.of(2022,10,22),LocalDate.of(2000,10,23));
//        Contract contractWithId1 = new Contract(1,"jkjdjd",LocalDate.of(2022,10,22),LocalDate.of(2000,10,23));
//        Contract contractWithId2 = new Contract(2,"zzzz",LocalDate.of(2022,10,22),LocalDate.of(2000,10,23));
//        List<Contract> contractList = List.of(contractWithId1,contractWithId2);
//        Mockito.doReturn(contractList).when(contractService).findAllContract();
//        mockMvc.perform(MockMvcRequestBuilders
//                .get("/api/public/contract/")
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk())
//                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(2)))
//                .andExpect(MockMvcResultMatchers.jsonPath("$[:1].name").value(contractWithId1.getName()));
//    }
//
//    @WithMockUser(authorities = { "ROLE_USER" })
//    @Test
//    void getContract() throws Exception {
//        Contract contractWithId1 = new Contract(1,"jkjdjd",LocalDate.of(2022,10,22),LocalDate.of(2000,10,23));
//        Optional<Contract> contractOptional = Optional.of(contractWithId1);
//        Mockito.doReturn(contractOptional).when(contractService).findContract(contractWithId1.getId());
//        mockMvc.perform(MockMvcRequestBuilders
//                .get("/api/public/contract/"+contractWithId1.getId())
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk())
//                .andExpect(MockMvcResultMatchers.jsonPath("$",notNullValue()))
//                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(contractWithId1.getName()));
//    }
//
//    @WithMockUser(authorities = { "ROLE_USER" })
//    @Test
//    void addContract() throws Exception {
//        Contract contract = new Contract("jkjdjd", LocalDate.of(2022,10,22),LocalDate.of(2000,10,23));
//        Contract contractWithId1 = new Contract(1,"jkjdjd",LocalDate.of(2022,10,22),LocalDate.of(2000,10,23));
//        Mockito.doReturn(contractWithId1).when(contractService).addContract(contract);
//        mockMvc.perform(MockMvcRequestBuilders
//                        .post("/api/public/contract/")
//                        .contentType(MediaType.APPLICATION_JSON)
//                        .content(objectMapper.writeValueAsString(contract)))
//                        .andExpect(status().isOk())
//                        .andExpect(MockMvcResultMatchers.jsonPath("$",notNullValue()))
//                        .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(contractWithId1.getName()));
//    }
//
//    @WithMockUser(authorities = { "ROLE_USER" })
//    @Test
//    void updateContract() throws Exception {
//        Contract contract = new Contract("jkjdjd", LocalDate.of(2022,10,22),LocalDate.of(2000,10,23));
//        Contract contractWithId1 = new Contract(1,"jkjdjd",LocalDate.of(2022,10,22),LocalDate.of(2000,10,23));
//        Mockito.doReturn(contractWithId1).when(contractService).addContract(contract);
//        mockMvc.perform(MockMvcRequestBuilders
//                .put("/api/public/contract/")
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(objectMapper.writeValueAsString(contract)))
//                .andExpect(status().isOk())
//                .andExpect(MockMvcResultMatchers.jsonPath("$",notNullValue()))
//                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(contractWithId1.getName()));
//    }
//
//    @WithMockUser(authorities = { "ROLE_USER" })
//    @Test
//    void deleteContract() throws Exception {
//        Contract contractWithId1 = new Contract(1,"jkjdjd",LocalDate.of(2022,10,22),LocalDate.of(2000,10,23));
//        mockMvc.perform(MockMvcRequestBuilders
//                .delete("/api/public/contract/"+contractWithId1.getId())
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk());
//        Mockito.verify(contractService,Mockito.times(1)).deleteContract(contractWithId1.getId());
//    }
//
//    @Test
//    void errorResponseEntity() throws Exception {
//        Contract contractWithId1 = new Contract(1,"jkjdjd",LocalDate.of(2022,10,22),LocalDate.of(2000,10,23));
//
//        mockMvc.perform(MockMvcRequestBuilders
//        .get("/api/public/contract/2")
//        .contentType(MediaType.APPLICATION_JSON)
//        .with(httpBasic("user", "TriniTron400"))
//        )
//        .andExpect(status().isBadRequest());
//
//        mockMvc.perform(MockMvcRequestBuilders
//                .get("/api/public/contract/2")
//                .contentType(MediaType.APPLICATION_JSON)
//                .with(httpBasic("user", "TriniTron500"))
//        )
//                .andExpect(status().isUnauthorized());
//
//        mockMvc.perform(MockMvcRequestBuilders
//                .get("/api/public/contract/2")
//                .contentType(MediaType.APPLICATION_JSON)
//                .with(httpBasic("admin", "TriniTron400"))
//        )
//                .andExpect(status().isForbidden());
//    }
//}