package ru.khvsoft.WOM.repo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import ru.khvsoft.WOM.entity.Company;
import ru.khvsoft.WOM.entity.TypeContract;
import ru.khvsoft.WOM.entity.Users;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class TypeContractRepoTest {

@Autowired
TypeContractRepo typeContractRepo;

@Autowired
TestEntityManager testEntityManager;

@Test
void addTypeContract(){
    TypeContract typeContract = new TypeContract("fjkjf", LocalDate.of(2020,10,2));
    assertEquals(typeContract,typeContractRepo.save(typeContract));
}

@Test
void findTypeContractById(){
    TypeContract typeContract = new TypeContract("fjkjf", LocalDate.of(2020,10,2));
    testEntityManager.persist(typeContract);
    long id = typeContract.getId();
    assertEquals(typeContract,typeContractRepo.findById(id).get());
}

@Test
void findAllTypeContract(){
    TypeContract typeContract1 = new TypeContract("fjkjf", LocalDate.of(2020,10,2));
    TypeContract typeContract2 = new TypeContract("fff", LocalDate.of(2020,10,2));
    testEntityManager.persist(typeContract1);
    testEntityManager.persist(typeContract2);
    assertEquals(2,typeContractRepo.findAll().size());
}

@Test
void deleteTypeContract(){
    TypeContract typeContract1 = new TypeContract("fjkjf", LocalDate.of(2020,10,2));
    testEntityManager.persist(typeContract1);
    long id = typeContract1.getId();
    typeContractRepo.deleteById(id);
    assertTrue(typeContractRepo.findById(id).isEmpty());
}

@Test
void getTypeContractWhereCompanySelected(){
    Company company = new Company("companyOk");
    Users user = new Users("user","1234567",company);
    company.addUser(user);
    testEntityManager.persist(company);
    testEntityManager.persist(user);
    TypeContract typeContract = new TypeContract("fjkjf", LocalDate.of(2020,10,2),company);
    testEntityManager.persist(typeContract);
    List<TypeContract> typeContractList = typeContractRepo.getTypeContractWhereCompanySelected(user.getName());
    assertEquals(1,typeContractList.size());
    assertEquals(typeContractList.get(0),typeContract);
}
}