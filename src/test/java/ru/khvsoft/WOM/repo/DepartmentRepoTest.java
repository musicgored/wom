package ru.khvsoft.WOM.repo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import ru.khvsoft.WOM.entity.Company;
import ru.khvsoft.WOM.entity.Department;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class DepartmentRepoTest {

@Autowired
DepartmentRepo departmentRepo;

@Autowired
TestEntityManager entityManager;

@Test
void addDepartment(){
    Department department = new Department("dfsdf");
    assertEquals(department,departmentRepo.save(department));
}

@Test
void findDepartmentById(){
    Department department = new Department("dfdf");
    entityManager.persist(department);
    long id = department.getId();
    assertEquals(department,departmentRepo.findById(id).get());
}

@Test
void findAllDepartment(){
    Department department1 = new Department("dfdf");
    Department department2 = new Department("fdff");
    entityManager.persist(department1);
    entityManager.persist(department2);
    assertEquals(2,departmentRepo.findAll().size());
}

@Test
void deleteDepartmentById(){
    Department department = new Department("dfdf");
    entityManager.persist(department);
    long id = department.getId();
    departmentRepo.deleteById(id);
    assertTrue(departmentRepo.findById(id).isEmpty());
}

@Test
void getDepartmentWhereCompanyId(){
    Department department = new Department("dfdf");
    Company company = new Company("name");
    entityManager.persist(company);
    department.setCompany(company);
    entityManager.persist(department);
    List<Department> departmentList = departmentRepo.getDepartmentWhereCompanyId(company.getId());
    assertEquals(1,departmentList.size());
    assertEquals(department,departmentList.get(0));
}

}