package ru.khvsoft.WOM.repo;

import org.checkerframework.checker.units.qual.A;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import ru.khvsoft.WOM.entity.Contract;
import ru.khvsoft.WOM.entity.ContractWork;
import ru.khvsoft.WOM.entity.Employee;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class ContractWorkRepoTest {

    @Autowired
    ContractWorkRepo contractWorkRepo;

    @Autowired
    TestEntityManager entityManager;

    @Test
    void selectContactWorkWhereEmployeeId() {
    Employee employee = new Employee("Evgeny","Vitalievich","Khapuzhenkov");
    entityManager.persist(employee);
    Contract contract1 = new Contract("work_of_nzmp", LocalDate.of(2020,10,20), LocalDate.of(2021,02,11));
    entityManager.persist(contract1);
    Contract contract2 = new Contract("work_of_ups", LocalDate.of(2019,10,20), LocalDate.of(2021,02,11));
    entityManager.persist(contract2);
    ContractWork contractWork1 = new ContractWork(LocalDate.of(2020,10,9),employee,contract1);
    ContractWork contractWork2 = new ContractWork(LocalDate.of(2020,10,11),employee,contract2);
    entityManager.persist(contractWork1);
    entityManager.persist(contractWork2);
    contractWorkRepo.selectContactWorkWhereEmployeeId(employee.getId());
    assertEquals(2,contractWorkRepo.selectContactWorkWhereEmployeeId(employee.getId()).size());
    }

    @Test
    void selectContractWorkWhereEmployeeIdAndDateWork(){
        Employee employee = new Employee("Evgeny","Vitalievich","Khapuzhenkov");
        entityManager.persist(employee);
        Contract contract = new Contract("work_of_nzmp", LocalDate.of(2020,10,20), LocalDate.of(2021,02,11));
        entityManager.persist(contract);
        ContractWork contractWork = new ContractWork(LocalDate.of(2020,10,9),employee,contract);
        entityManager.persist(contractWork);
        assertEquals(1,contractWorkRepo.selectContractWorkWhereEmployeeIdAndDateWork(employee.getId(),LocalDate.of(2020,10,9)).size());
    }

    @Test
    void selectContractWorkBetweenStartDateAndEndDate(){
        Employee employee = new Employee("Evgeny","Vitalievich","Khapuzhenkov");
        entityManager.persist(employee);
        Contract contract = new Contract("work_of_nzmp", LocalDate.of(2020,10,20), LocalDate.of(2021,02,11));
        entityManager.persist(contract);
        ContractWork contractWork = new ContractWork(LocalDate.of(2020,10,9),employee,contract);
        entityManager.persist(contractWork);
        assertEquals(1,contractWorkRepo.selectContractWorkBetweenStartDateAndEndDate(LocalDate.of(2020,10,9),LocalDate.of(2020,10,20)).size());
        assertEquals(0,contractWorkRepo.selectContractWorkBetweenStartDateAndEndDate(LocalDate.of(2020,10,7),LocalDate.of(2020,10,8)).size());
        assertEquals(0,contractWorkRepo.selectContractWorkBetweenStartDateAndEndDate(LocalDate.of(2020,10,10),LocalDate.of(2020,10,11)).size());
    }
}