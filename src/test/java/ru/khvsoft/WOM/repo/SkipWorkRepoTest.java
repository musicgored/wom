package ru.khvsoft.WOM.repo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import ru.khvsoft.WOM.classes.ReasonSkip;
import ru.khvsoft.WOM.entity.Employee;
import ru.khvsoft.WOM.entity.SkipWork;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class SkipWorkRepoTest {

@Autowired
private TestEntityManager entityManager;

@Autowired
private SkipWorkRepo skipWorkRepo;

@Test
void addSkipWork(){
    SkipWork skipWork = new SkipWork(ReasonSkip.VACATION, LocalDate.of(2020,01,02),LocalDate.of(2020,02,11));
    SkipWork skipWorkSave = skipWorkRepo.save(skipWork);
    assertEquals(skipWork,skipWorkSave);
}

@Test
void findSkipWorkById(){
    SkipWork skipWork = skipWorkRepo.save(new SkipWork(ReasonSkip.MEDICAL, LocalDate.of(2021,01,02),LocalDate.of(2022,02,11)));
    long id = skipWork.getId();
    assertEquals(skipWork,skipWorkRepo.findById(id).get());
}

@Test
void findAllSkipWork(){
    entityManager.persist(new SkipWork(ReasonSkip.MEDICAL, LocalDate.of(2021,01,02),LocalDate.of(2022,02,11)));
    entityManager.persist(new SkipWork(ReasonSkip.VACATION, LocalDate.of(2022,05,03),LocalDate.of(2022,05,12)));
    assertEquals(2,skipWorkRepo.findAll().size());
}

@Test
void deleteSkipWorkById(){
    SkipWork skipWork = new SkipWork(ReasonSkip.MEDICAL, LocalDate.of(2021,01,02),LocalDate.of(2022,02,11));
    entityManager.persist(skipWork);
    long id = skipWork.getId();
    skipWorkRepo.deleteById(id);
    assertTrue(skipWorkRepo.findById(id).isEmpty());
}

@Test
void selectSkipWorkWhereReasonSkip(){
    SkipWork skipWork = new SkipWork(ReasonSkip.MEDICAL, LocalDate.of(2021,01,02),LocalDate.of(2022,02,11));
    entityManager.persist(skipWork);
    assertEquals(1,skipWorkRepo.selectSkipWorkWhereReasonSkip(ReasonSkip.MEDICAL).size());
    assertEquals(0,skipWorkRepo.selectSkipWorkWhereReasonSkip(ReasonSkip.VACATION).size());
}

@Test
void  selectSkipWorkWhereEmployeeIdAndReasonSkip(){
    Employee employee = new Employee("sdfsdf","sdfsdf","dasdasdasd");
    entityManager.persist(employee);
    SkipWork skipWork = new SkipWork(ReasonSkip.MEDICAL, employee,LocalDate.of(2021,01,02),LocalDate.of(2022,02,11));
    entityManager.persist(skipWork);
    assertEquals(1,skipWorkRepo.selectSkipWorkWhereEmployeeIdAndReasonSkip(employee.getId(),skipWork.getReasonSkip()).size());
    assertEquals(0,skipWorkRepo.selectSkipWorkWhereEmployeeIdAndReasonSkip(employee.getId(),ReasonSkip.VACATION).size());
}

}