package ru.khvsoft.WOM.repo;

import net.bytebuddy.asm.Advice;
import org.checkerframework.checker.units.qual.A;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import ru.khvsoft.WOM.entity.Users;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class UserRepoTest {

    @Autowired
    UserRepo userRepo;

    @Autowired
    TestEntityManager entityManager;

    @Test
    void existUserInDatabase() {
    Users user = new Users("fdfdf","dfsdf");
    Users otherUser = new Users("fdfdf","dfsdf");
    entityManager.persist(user);
    assertFalse(userRepo.existUserInDatabase(otherUser.getName()).isEmpty());
    }

    @Test
    void addUser(){
        Users user = new Users("fdfdf","dfsdf");
        assertEquals(user,userRepo.save(user));
    }

    @Test
    void findUserById(){
        Users user = new Users("fdfdf","dfsdf");
        entityManager.persist(user);
        long id = user.getId();
        assertEquals(user,userRepo.findById(id).get());
    }

    @Test
    void findUserAll(){
        Users user1 = new Users("fdfdf","dfsdf");
        Users user2 = new Users("fdfdf","ddddd");
        entityManager.persist(user1);
        entityManager.persist(user2);
        assertEquals(2,userRepo.findAll().size());
    }

    @Test
    void deleteUserById(){
        Users user1 = new Users("fdfdf","dfsdf");
        entityManager.persist(user1);
        long id = user1.getId();
        userRepo.deleteById(id);
        assertTrue(userRepo.findById(id).isEmpty());
    }

    @Test
    void getUserByUserName(){
        Users user1 = new Users("user","dfsdf");
        entityManager.persist(user1);
        assertNotEquals(null,userRepo.getUserByUserName("user"));
    }
}