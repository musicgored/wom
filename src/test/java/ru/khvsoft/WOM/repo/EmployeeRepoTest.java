package ru.khvsoft.WOM.repo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import ru.khvsoft.WOM.entity.Contract;
import ru.khvsoft.WOM.entity.ContractWork;
import ru.khvsoft.WOM.entity.Employee;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class EmployeeRepoTest {

@Autowired
EmployeeRepo employeeRepo;

@Autowired
TestEntityManager entityManager;
@Test
void addEmployee(){
    Employee employee = new Employee("fgdfg","dfsdfsdf","sdfsdf");
    assertEquals(employee,employeeRepo.save(employee));
}

@Test
void findEmployeeById(){
    Employee employee = new Employee("fgdfg","dfsdfsdf","sdfsdf");
    entityManager.persist(employee);
    long id = employee.getId();
    assertEquals(employee,employeeRepo.findById(id).get());
}

@Test
void findAllEmployee(){
    Employee employee1 = new Employee("fgdfg","fgdfg","sdfsdf");
    Employee employee2 = new Employee("fgdfg","dfgfg","sdfdfgdfgsdf");
    entityManager.persist(employee1);
    entityManager.persist(employee2);
    assertEquals(2,employeeRepo.findAll().size());
}

@Test
void deleteEmployeeById(){
    Employee employee = new Employee("fgdfg","fgdfg","sdfsdf");
    entityManager.persist(employee);
    long id = employee.getId();
    employeeRepo.deleteById(id);
    assertTrue(employeeRepo.findById(id).isEmpty());
}

//@Test
//void selectContactWorkWhereEmployeeId(){
//    Employee employee = new Employee("Evgeny","Vitalievich","Khapuzhenkov");
//    entityManager.persist(employee);
//    Contract contract = new Contract("work_of_nzmp", LocalDate.of(2020,10,20), LocalDate.of(2021,02,11));
//    entityManager.persist(contract);
//    ContractWork contractWork = new ContractWork(LocalDate.of(2020,10,9),employee,contract);
//    entityManager.persist(contractWork);
//    assertEquals(employee,employeeRepo.selectContactWorkWhereEmployeeId(employee.getId()).get());
//}
}