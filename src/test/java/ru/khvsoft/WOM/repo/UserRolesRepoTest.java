package ru.khvsoft.WOM.repo;

import org.checkerframework.checker.units.qual.A;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import ru.khvsoft.WOM.classes.NameRole;
import ru.khvsoft.WOM.entity.UsersRole;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class UserRolesRepoTest {

@Autowired
UserRolesRepo userRolesRepo;

@Autowired
TestEntityManager testEntityManager;

@Test
void addUserRole(){
    UsersRole usersRole = new UsersRole(NameRole.ROLE_ADMIN);
    assertEquals(usersRole,userRolesRepo.save(usersRole));
}

@Test
void findUserRoleById(){
    UsersRole usersRole = new UsersRole(NameRole.ROLE_ADMIN);
    testEntityManager.persist(usersRole);
    long id = usersRole.getId();
    assertEquals(usersRole,userRolesRepo.findById(id).get());
}

@Test
void findAllUserRole(){
    UsersRole usersRole1 = new UsersRole(NameRole.ROLE_ADMIN);
    UsersRole usersRole2 = new UsersRole(NameRole.ROLE_USER);
    testEntityManager.persist(usersRole1);
    testEntityManager.persist(usersRole2);
    assertEquals(2,userRolesRepo.findAll().size());
}

@Test
void deleteUserRoleById(){
    UsersRole usersRole1 = new UsersRole(NameRole.ROLE_ADMIN);
    testEntityManager.persist(usersRole1);
    long id = usersRole1.getId();
    userRolesRepo.deleteById(id);
    assertTrue(userRolesRepo.findById(id).isEmpty());
}

}