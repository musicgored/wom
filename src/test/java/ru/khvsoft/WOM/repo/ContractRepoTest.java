package ru.khvsoft.WOM.repo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import ru.khvsoft.WOM.entity.Company;
import ru.khvsoft.WOM.entity.Contract;
import ru.khvsoft.WOM.entity.Users;
import ru.khvsoft.WOM.service.UsersService;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class ContractRepoTest {

@Autowired
TestEntityManager entityManager;

@Autowired
ContractRepo contractRepo;


@Test
void addContract(){
    Contract contract = new Contract("kjdjkd", LocalDate.of(2020,10,20), LocalDate.of(2021,02,11));
    assertEquals(contract,contractRepo.save(contract));
}

@Test
void findContractById(){
    Contract contract = new Contract("kjdjkd", LocalDate.of(2020,10,20), LocalDate.of(2021,02,11));
    entityManager.persist(contract);
    long id = contract.getId();
    assertEquals(contract,contractRepo.findById(id).get());
}

@Test
void findAllContract(){
    Contract contract1 = new Contract("kjdjkd", LocalDate.of(2020,10,20), LocalDate.of(2021,02,11));
    Contract contract2 = new Contract("dffff", LocalDate.of(2020,10,20), LocalDate.of(2021,02,11));
    entityManager.persist(contract1);
    entityManager.persist(contract2);
    assertEquals(2,contractRepo.findAll().size());
}

@Test
void deleteContract(){
    Contract contract = new Contract("kjdjkd", LocalDate.of(2020,10,20), LocalDate.of(2021,02,11));
    entityManager.persist(contract);
    long id = contract.getId();
    contractRepo.deleteById(id);
    assertTrue(contractRepo.findById(id).isEmpty());
}

@Test
void getContractWhereCompanyId(){
    Contract contract = new Contract("kjdjkd", LocalDate.of(2020,10,20), LocalDate.of(2021,02,11));
    Company company = new Company("name");
    contract.setCompany(company);
    entityManager.persist(company);
    entityManager.persist(contract);
    List <Contract> contractList = contractRepo.getContractWhereCompanyId(company.getId());
    assertEquals(1,contractList.size());
    assertEquals(contract,contractList.get(0));
}


}