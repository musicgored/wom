requirejs.config({
    baseUrl: 'js'
})

function buildRoute(view) {
    return function () {
        webix.ui({
            id:'root',
            type:"space",
            rows:[
                view
            ]
        },$$("root"))
    }
}

// function buildButton(label,route,openModule){
//     return {
//         view:'button',
//         value:label,
//         width:100,
//         align:"left",
//         click:function () {
//             openModule.mod.forEach(m=>m.destructor())
//             openModule.mod.clear()
//             routie(route)
//         }
//     }
//
// }

require(['views/main','views/typeContract','views/contract','util/resourceProxy','util/openModule','views/department','views/employee','views/contractWork','views/skipWork','views/workSchedule','views/reportsWork'],
    function (main,typeContract,contract,resourceProxy,openModule,department,employee,contractWork,skipWork,workSchedule,reportsWork) {
    webix.ready(function(){
        webix.ui({
            container:'app',
            rows:[
                {view:'toolbar', id:'mainToolBar', type:"space",
                    cols:[
                    // buildButton('Main','',openModule),
                        {
                            view:"menu",
                            submenuConfig:{
                                width:300,
                                template:"#value#",
                            },
                            data:[
                                // {
                                //     value:"Домой", id:1, type:"icon", icon: "wxi-dots"
                                // },
                                {
                                    value:"Работы", id:2, type:"icon", icon: "wxi-clock"
                                },
                                {
                                    value:"Пропуски", id:3, type:"icon", icon: "wxi-sync"
                                },
                                {
                                    value:"Отчёты", type:"icon", icon: "wxi-sync",
                                    config:{  width:180 },
                                    submenu:[
                                        {id:41,value:"Отчёт по сотрудникам"},
                                    ]
                                },
                                {
                                    value:"Справочники", type:"icon", icon:"wxi-folder-open",
                                    config:{  width:180 },
                                    submenu:[
                                        {id:11,value:"Отделы"},
                                        {id:12,value:"Типы договоров"},
                                        {id:13,value:"Договора"},
                                        {id:14,value:"Сотрудники"},
                                        {id:15,value:"Графики работы"},
                                    ]
                                }
                            ],
                            type:{
                                subsign:true
                            },
                            on:{
                                onMenuItemClick:function(id){
                                    openModule.mod.forEach(m=>m.destructor())
                                    openModule.mod.clear()
                                    switch (id) {
                                        case '1':
                                            routie('')
                                            break;
                                        case '2':
                                            routie('contractWork')
                                            break;
                                        case '3':
                                            routie('skipWork')
                                            break;
                                        case '11':
                                            routie('department')
                                            break;
                                        case '12':
                                            routie('typeContract');
                                            break;
                                        case '13':
                                            routie('contract');
                                            break;
                                        case '14':
                                            routie('employee');
                                            break;
                                        case '15':
                                            routie('workSchedule');
                                            break;
                                        case '41':
                                            routie('reportsWork');
                                            break;
                                    }
                                }
                            }

                        },

                        {view:'dataview',
                            select:true,
                            type: {
                                height: 60,
                                width:"auto"
                            },
                            template:"<div class='webix_strong'>Вы вошли как: #userName#</div>",
                            id:'mainInfo',
                            url:"resource->api/public/user/"
                        },
                    ]
                },
                // {view:"select", id:"selectCompany" , value:215, label:"<div class='webix_strong'>Организация</div>",options:'resource->/api/public/company/items'},
                {
                    id:'root',
                }
            ]
        });
    });

    routie({
        '':buildRoute(main),
        'typeContract':buildRoute(typeContract),
        'contract':buildRoute(contract),
        'department':buildRoute(department),
        'employee':buildRoute(employee),
        'contractWork':buildRoute(contractWork),
        'skipWork':buildRoute(skipWork),
        'workSchedule':buildRoute(workSchedule),
        'reportsWork':buildRoute(reportsWork)
    })
})

