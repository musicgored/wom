define(['collections/reasonSkip','collections/contract','collections/employee','util/openModule'],function (reasonSkip,contract,employee,openModule) {

        function removeElement(id){
            $$("contractWork").remove(id);
        }

        function customCheckbox(obj, common, value){
            if (obj.contract!=null) {
                message = contract.getItem(value).value
            } else {message = "Не выбрано#"+"#C0C0C0"}
                let arr = message.split('#');
                return `<span style="background-color:#${arr[2]};border-radius:4px; padding-right:10px;">&nbsp</span> ${arr[0]}`;
        }

        function customCheckboxSkip(obj, common, value){
            let color = "C0C0C0";
            if (obj.reasonSkip!=null) {
                message = reasonSkip.getItem(value).value

                switch(obj.reasonSkip) {
                    case 'NOREASON':  color = "800000"
                        break

                    case 'MEDICAL':    color = "00FFFF"
                        break

                    case 'WEEKEND':    color = "008000"
                        break

                    case 'VACATION':    color = "FFFF00"
                        break
                }

            }
            return `<span style="background-color:#${color};border-radius:4px; padding-right:10px;">&nbsp</span> ${message}`;
        }

        function mark_votes(value){

        if (value!='')
            return { "background-color":"#FFAAAA" };
        };

         var ajax = webix.ajax().headers({
        'Content-type': 'application/json'
         })

        var format = webix.Date.dateToStr("%Y-%m-%d")

        var format2 = webix.Date.dateToStr("%d-%m-%Y")

        var url = "api/public/employee/contract_work/";

        var changeUrl = url+format(new Date(Date.now()));


        return {
            type:"space",
            rows: [
                {
                    view: 'toolbar',
                    id:'contractWorkToolbar',
                    cols:[

                        {
                            id:"datepicker",
                            view:"calendar",
                            weekHeader:true,
                            events:webix.Date.isHoliday,
                            value:new Date(Date.now()),
                            on:{
                                onChange: function(newValue){
                                    changeUrl=url+format(newValue[0])
                                    $$("contractWork").clearAll()
                                    $$("contractWork").load(changeUrl,'json')
                                    $$("viewSkipWork").clearAll()
                                },
                            }
                        },


                        {
                            view:"form", scroll:false, width:320, id:'datepickerFill' ,
                            elements:[
                                { view:"datepicker", label:"Дата", name:"start", value:new Date(Date.now()), stringResult:true },
                                { view:"button", value:"Заполнить как этот день", click:function(){
                                        ajax.put(changeUrl+"/"+ webix.i18n.parseFormatStr(this.getParentView().getValues().start).substring(0,10), this.getParentView().getValues())
                                        $$("contractWork").clearAll()
                                        $$("contractWork").load(changeUrl,'json')
                                        $$("viewSkipWork").clearAll()
                                }},

                                { view:"button", value:"Очистить", click:function(){
                                        ajax.del(changeUrl)
                                        $$("contractWork").clearAll()
                                        $$("contractWork").load(changeUrl,'json')
                                        $$("viewSkipWork").clearAll()
                                    }
                                },

                                { view:"button", value:"Типовое заполнение", click:function(){
                                        ajax.put(changeUrl)
                                        // $$("contractWork").clearAll()
                                        // $$("contractWork").load(changeUrl,'json')
                                        // $$("viewSkipWork").clearAll()
                                    }
                                }

                            ]
                            // id:"datepickerFill",
                            // view:"datepicker",
                            // height: 10,
                            // weekHeader:true,
                            // events:webix.Date.isHoliday,
                            // date:new Date(Date.now()),
                            // on:{
                            //     onChange: function(newValue){
                            //         // changeUrl=url+format(newValue[0])
                            //         // $$("contractWork").clearAll()
                            //         // $$("contractWork").load(changeUrl,'json')
                            //         // $$("viewSkipWork").clearAll()
                            //     },
                            // }
                        },

                        //
                        // {
                        //     id:"template", template:function(obj){
                        //         return format2(obj.value);
                        //     },
                        //     height: 10
                        // },


                        {
                            view: "datatable",
                            id:'viewSkipWork',
                            columns: [
                            {id: "employee", header: "Сотрудник", fillspace: true, collection: employee, sort:"string"},
                            {id: "reasonSkip", header: "Причина пропуска", fillspace: true, collection: reasonSkip,template:customCheckboxSkip},
                            ]
                        },
                        // {
                        //     view:'button',
                        //     label:'Добавить',
                        //     click:function () {
                        //         var contactList = $$('contractWork')
                        //         var id = contactList.add({
                        //
                        //         })
                        //         contactList.editRow(id)
                        //     }
                        // },
                    ]
                },
                {
                    view: "datatable",
                    pager:'contractWorkPages',
                    height:200,
                    scrollY:false,
                    id: "contractWork",
                    editable: true,
                    columns: [
                        {id:"index", header:"", width:50 },
                        {id: "employee", header: ["Сотрудник",{content:"selectFilter"}], fillspace: true, collection: employee,sort:"string"},
                        {id: "contract", header: ["Договор",{content:"selectFilter"}], fillspace: true, collection: contract, editor: "combo",sort:"string",template:customCheckbox},
                        {id: "reasonSkip", header: "Пропуск", fillspace: true, collection: reasonSkip,cssFormat:mark_votes,hidden:true},
                    ],
                    rules:{
                        // contract: function(value){
                        //     console.log("---------------------------------")
                        //     if (this.reasonSkip!='') {
                        //         return value == ''
                        //         console.log("1")
                        //     }
                        //     return value != ''
                        //     console.log("2")
                        // },
                        employee: webix.rules.isNotEmpty,
                    },
                    scheme:{
                        $init: function(obj) {
                            obj.index = this.count();
                        },
                        $update: function (obj) {
                            obj.dateWork = webix.i18n.parseFormatStr(obj.dateWork).substring(0,10)
                            ajax.post(changeUrl , obj)
                        },
                        $save: function (obj) {
                            obj.dateWork = webix.i18n.parseFormatStr(obj.dateWork).substring(0,10)
                        }
                    },
                    onClick:{
                        "wxi-trash":(e,id) => removeElement(id),
                    },
                    autoheight: true,
                    minHeight: 300,
                    scroll: "auto",
                    on: {
                        onBeforeRender: function () {

                        },
                        onBeforeEditStop:function(state, editor, ignore){
                            var check = ( editor.getValue() != "" );
                            if (!ignore && !check){
                                webix.message(editor.column+" Поле не должно быть пустым");
                                return false;
                            }
                        },
                        onBeforeLoad: function () {
                            this.showOverlay("Загрузка...");
                        },
                        onAfterLoad: function () {
                            this.hideOverlay();
                            openModule.mod.add($$('datepicker'))
                            openModule.mod.add($$('contractWork'))
                            openModule.mod.add($$('contractWorkPages'))
                            openModule.mod.add($$('contractWorkToolbar'))
                            // $$("template").bind($$("datepicker"));
                            this.filter(function (object) {
                                if (object.reasonSkip!=null) {
                                    $$('viewSkipWork').add(object)
                                }
                                return object.reasonSkip==null;
                            })
                        }
                    },
                    // url: changeUrl
                },
                {
                    view:'pager',
                    id:"contractWorkPages",
                    template:"{common.prev()} {common.pages()} {common.next()}",
                    size:20,
                    group:5
                }
            ]
        }

    }
)