define(['collections/typeContract','util/openModule'],function (typeContract,openModule) {

    function removeElement(id){
            $$("contract").remove(id);
    }

    return {
        type:"space",
        rows: [
                    {
                        view: 'toolbar',
                        id:'contractToolbar',
                        cols:[
                            {
                                view:'button',
                                label:'Добавить',
                                click:function () {
                                    var contactList = $$('contract')
                                    var id = contactList.add({

                                    })
                                    contactList.editRow(id)
                                }
                            },
                        ]
                    },
                    {
                        view: "datatable",
                        id: "contract",
                        editable: true,
                        pager:"contractPages",
                        columns: [
                            {id: "name", header:["Название договора", {content:"textFilter"}], fillspace: true, editor: "text"},
                            {id:"color", header:"Color", editor:"color",template:`<span style="background-color:#color#;border-radius:4px; padding-right:10px;">&nbsp</span>`},
                            {id: "numbre", header:["Номер договора", {content:"textFilter"}], fillspace: true, editor: "text"},
                            {id: "typeContract", header: ["Тип договора",{content:"selectFilter"}], fillspace: true, collection: typeContract, editor: "combo"},
                            {id:'start_date',editor: "date", header: "Дата начала", width: 120, format:webix.Date.dateToStr("%Y-%m-%d")},
                            {id:'end_date', editor: "date", header: "Дата конца", width: 120, format:webix.Date.dateToStr("%Y-%m-%d")},
                            { header:[""], width:50, template:"{common.trashIcon()}" }
                        ],
                        rules:{
                            name: webix.rules.isNotEmpty,
                            color: webix.rules.isNotEmpty,
                            typeContract: webix.rules.isNotEmpty,
                            start_date: webix.rules.isNotEmpty,
                            end_date: webix.rules.isNotEmpty
                        },
                        scheme:{
                            $init: function() {

                            },
                            $update: function (obj) {
                                obj.start_date = webix.i18n.parseFormatStr(obj.start_date).substring(0,10)
                                obj.end_date = webix.i18n.parseFormatStr(obj.end_date).substring(0,10)
                            },
                            $save: function (obj) {
                                obj.start_date = webix.i18n.parseFormatStr(obj.start_date).substring(0,10)
                                obj.end_date = webix.i18n.parseFormatStr(obj.end_date).substring(0,10)
                            }
                        },
                        onClick:{
                            "wxi-trash":(e,id) => removeElement(id),
                        },
                        autoheight: true,
                        minHeight: 300,
                        scroll: "auto",
                        on: {
                                            onBeforeLoad: function () {
                                                this.showOverlay("Загрузка...");
                                            },
                                            onAfterLoad: function () {
                                                this.hideOverlay();
                                                openModule.mod.add($$('contract'))
                                                openModule.mod.add($$('contractPages'))
                                                openModule.mod.add($$('contractToolbar'))
                                            }
                                        },
                        url: "resource->api/public/contract/",
                        save: "resource->api/public/contract/"
                    },
                     {
                         view:'pager',
                         id:"contractPages",
                         template:"{common.prev()} {common.pages()} {common.next()}",
                         size:15,
                         group:5
                     },
                ]
    }

}
)