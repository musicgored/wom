define(['util/openModule'],function (openModule) {
    function removeElement(id){
        $$("workSchedule").remove(id);
    }

    function customCheckbox(obj, common, value){
        if(value){
            return "<span class='webix_table_checkbox checked'> ДА </span>";
        }else{
            return "<span class='webix_table_checkbox notchecked'> НЕТ </span>";
        }
    }

    return {
        type:"space",
        rows: [
            {
                view: 'toolbar',
                id: 'workScheduleToolbar',
                cols:[
                    {
                        view:'button',
                        label:'Добавить',
                        click:function () {
                            var typeContractList = $$('workSchedule')
                            var id = typeContractList.add({

                            })
                            typeContractList.editRow(id)
                        }
                    },
                ]
            },
            {
                view: "datatable", id: "workSchedule", editable: true,
                pager:"workSchedulePages",
                columns: [
                    {id: "name", header:["Название графика", {content:"textFilter"}], fillspace: true, editor: "text"},
                    {id: "quantity_work", header:"Количество дней работы", fillspace: true, numberFormat:"1'111", editor: "text"},
                    {id: "quantity_weekend", header:"Количество дней отдыха", fillspace: true, numberFormat:"1'111", editor: "text"},
                    {id: "work_weekend", header:"Работа в выходные", fillspace: true, editor:"checkbox", template:customCheckbox,},
                    { header:[""], width:50, template:"{common.trashIcon()}" }
                ],
                rules:{
                    name: webix.rules.isNotEmpty,
                    quantity_work: webix.rules.isNotEmpty,
                    quantity_weekend: webix.rules.isNotEmpty,
                },
                scheme:{
                    $init: function(){
                    },
                    $update: function (obj) {
                    },
                    $save: function (obj) {
                    }
                },
                onClick:{
                    "wxi-trash":(e,id) => removeElement(id),
                },
                autoheight: true,
                minHeight: 300,
                scroll: "auto",
                on: {
                    onBeforeLoad: function () {
                        this.showOverlay("Загрузка...");
                    },
                    onAfterLoad: function () {
                        this.hideOverlay();
                        openModule.mod.add($$("workSchedule"))
                        openModule.mod.add($$("workSchedulePages"))
                        openModule.mod.add($$("workScheduleToolbar"))
                    }
                },

                url: "resource->api/public/work_schedule/",
                save: "resource->api/public/work_schedule/"
            },
            {
                view:'pager',
                id:"workSchedulePages",
                template:"{common.prev()} {common.pages()} {common.next()}",
                size:20,
                group:5
            },
        ]
    }
})