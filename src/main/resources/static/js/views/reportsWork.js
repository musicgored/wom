define(['collections/reasonSkip','collections/contract','collections/employee','util/openModule'],function (reasonSkip,contract,employee,openModule) {


        var ajax = webix.ajax().headers({
            'Content-type': 'application/json'
        })

        var startDate = new Date(Date.now())

        var endDate = new Date(Date.now())

        var format = webix.Date.dateToStr("%Y-%m-%d")

        var format2 = webix.Date.dateToStr("%d-%m-%Y")

        var url = "api/public/employee/reports/";

        var changeUrl = url+format(new Date(startDate)+"/"+url+format(new Date(endDate)))



        function customViewNoReason(obj, common, value){
            let color = "C0C0C0";
            if (obj.noReason!=null&&obj.noReason!=0) {color = "800000";}
            return `<span style="background-color:#${color};border-radius:4px; padding-right:10px;">&nbsp</span> ${obj.noReason}`;
        }

    function customViewCountWeekend(obj, common, value){
        let color = "C0C0C0";
        if (obj.countWeekend!=null&&obj.countWeekend!=0) {color = "008000";}
        return `<span style="background-color:#${color};border-radius:4px; padding-right:10px;">&nbsp</span> ${obj.countWeekend}`;
    }

    function customViewCountVacation(obj, common, value){
        let color = "C0C0C0";
        if (obj.countVacation!=null&&obj.countVacation!=0) {color = "FFFF00";}
        return `<span style="background-color:#${color};border-radius:4px; padding-right:10px;">&nbsp</span> ${obj.countVacation}`;
    }

    function customViewCountMedical(obj, common, value){
        let color = "C0C0C0";
        if (obj.countMedical!=null&&obj.countMedical!=0) {color = "00FFFF";}
        return `<span style="background-color:#${color};border-radius:4px; padding-right:10px;">&nbsp</span> ${obj.countMedical}`;
    }


        return {
            type:"space",
            rows: [
                {
                    view: 'toolbar',
                    id:'reportsWorkToolbar',
                    cols:[
                        {
                            id:"datepicker",
                            view:"calendar",
                            weekHeader:true,
                            events:webix.Date.isHoliday,
                            value:new Date(Date.now()),
                            on:{
                                onChange: function(newValue){
                                    startDate = newValue[0];
                                    changeUrl=url+format(startDate)+"/"+format(endDate)
                                    $$("reportsWork").clearAll()
                                    $$("reportsWork").load(changeUrl,'json')
                                },
                            }
                        },
                        {
                            id:"datepickerEnd",
                            view:"calendar",
                            weekHeader:true,
                            events:webix.Date.isHoliday,
                            value:new Date(Date.now()),
                            on:{
                                onChange: function(newValue){
                                    endDate = newValue[0];
                                    changeUrl=url+format(startDate)+"/"+format(endDate)
                                    $$("reportsWork").clearAll()
                                    $$("reportsWork").load(changeUrl,'json')
                                },
                            }
                        },
                        {
                            id:"template1", template:function(obj){
                                let date = format2(obj.value);
                                return `<p>Выбранная начальная дата: </p> <p style="background-color: #3ea9d9;font-weight: bold; border-radius:4px">${date}</p>`
                            },
                            height: 120
                        },
                        {
                            id:"template2", template:function(obj){
                                // return format2(obj.value);
                                let date = format2(obj.value);
                                return `<p>Выбранная конечная дата: </p> <p style="background-color: #3ea9d9;font-weight: bold; border-radius:4px">${date}</p>`
                            },
                            height: 120
                        }
                    ]
                },
                {
                    view: "datatable",
                    pager:'reportsWorkPages',
                    height:200,
                    scrollY:false,
                    id: "reportsWork",
                    editable: true,
                    columns: [
                        {id:"index", header:"", width:50 },
                        {id: "employee", header: ["Сотрудник",{content:"selectFilter"}], fillspace: true, collection: employee,sort:"string"},
                        {id: "noReason", header: ["За свой счёт",{content:"selectFilter"}], fillspace: true, sort:"string",template:customViewNoReason},
                        {id: "countMedical", header: ["Больничные",{content:"selectFilter"}], fillspace: true, sort:"string",template:customViewCountMedical},
                        {id: "countVacation", header: ["Отпуск",{content:"selectFilter"}], fillspace: true, sort:"string",template:customViewCountVacation},
                        {id: "countWeekend", header: ["Выходные",{content:"selectFilter"}], fillspace: true, sort:"string",template:customViewCountWeekend},
                        {id: "countWork", header: ["Отработано дней",{content:"selectFilter"}], fillspace: true, sort:"string"},
                    ],
                    rules:{
                        employee: webix.rules.isNotEmpty,
                    },
                    scheme:{
                        $init: function(obj) {
                            obj.index = this.count();
                        },
                        $update: function (obj) {
                            obj.dateWork = webix.i18n.parseFormatStr(obj.dateWork).substring(0,10)
                            ajax.post(changeUrl , obj)
                        },
                        $save: function (obj) {
                            obj.dateWork = webix.i18n.parseFormatStr(obj.dateWork).substring(0,10)
                        }
                    },
                    autoheight: true,
                    minHeight: 300,
                    scroll: "auto",
                    on: {
                        onBeforeRender: function () {

                        },
                        onBeforeLoad: function () {
                            this.showOverlay("Загрузка...");
                        },
                        onAfterLoad: function () {
                            this.hideOverlay();
                            $$("template1").bind($$("datepicker"));
                            $$("template2").bind($$("datepickerEnd"));
                            openModule.mod.add($$('datepicker'))
                            openModule.mod.add($$('datepickerEnd'))
                            openModule.mod.add($$('reportsWork'))
                            openModule.mod.add($$('reportsWorkPages'))
                            openModule.mod.add($$('reportsWorkToolbar'))
                        }
                    },
                },
                {
                    view:'pager',
                    id:"reportsWorkPages",
                    template:"{common.prev()} {common.pages()} {common.next()}",
                    size:20,
                    group:5
                }
            ]
        }

    }
)