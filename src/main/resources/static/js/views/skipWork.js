define(['collections/reasonSkip','collections/employee','util/openModule'],function (reasonSkip,employee,openModule) {

        function removeElement(id){
            $$("skipWork").remove(id);
        }

        return {
            type:"space",
            rows: [
                {
                    view: 'toolbar',
                    id:'skipWorkToolbar',
                    cols:[
                        {
                            view:'button',
                            label:'Добавить',
                            click:function () {
                                var contactList = $$('skipWork')
                                var id = contactList.add({

                                })
                                contactList.editRow(id)
                            }
                        },
                    ]
                },

                {
                    view: "datatable",
                    id: "skipWork",
                    editable: true,
                    pager:"skipWorkPages",
                    columns: [
                        {id:"index", header:"", width:50 },
                        {id: "reasonSkip", header: ["Причина пропуска",{content:"selectFilter"}], fillspace: true, collection: reasonSkip, editor: "combo"},
                        {id: "employee", header: ["Сотрудник",{content:"selectFilter"}], fillspace: true, collection: employee, editor: "combo"},
                        {id:'startDate',editor: "date", header: "Дата начала", width: 120, format:webix.Date.dateToStr("%Y-%m-%d")},
                        {id:'endDate', editor: "date", header: "Дата конца", width: 120, format:webix.Date.dateToStr("%Y-%m-%d")},
                        { header:[""], width:50, template:"{common.trashIcon()}" }
                    ],
                    rules:{
                        reasonSkip: webix.rules.isNotEmpty,
                        employee: webix.rules.isNotEmpty,
                        startDate: webix.rules.isNotEmpty,
                        endDate: webix.rules.isNotEmpty
                    },
                    scheme:{
                        $init: function(obj) {
                            obj.index = this.count();
                        },
                        $update: function (obj) {
                            obj.startDate = webix.i18n.parseFormatStr(obj.startDate).substring(0,10)
                            obj.endDate = webix.i18n.parseFormatStr(obj.endDate).substring(0,10)
                        },
                        $save: function (obj) {
                            obj.startDate = webix.i18n.parseFormatStr(obj.startDate).substring(0,10)
                            obj.endDate = webix.i18n.parseFormatStr(obj.endDate).substring(0,10)
                        }
                    },
                    onClick:{
                        "wxi-trash":(e,id) => removeElement(id),
                    },
                    autoheight: true,
                    minHeight: 300,
                    scroll: "auto",
                    on: {
                        onBeforeLoad: function () {
                            this.showOverlay("Загрузка...");
                        },
                        onAfterLoad: function () {
                            this.hideOverlay();
                            openModule.mod.add($$('skipWork'))
                            openModule.mod.add($$('skipWorkPages'))
                            openModule.mod.add($$('skipWorkToolbar'))
                        }
                    },
                    url: "resource->api/public/skip_work/",
                    save: "resource->api/public/skip_work/"

                },
                {
                    view:'pager',
                    id:"skipWorkPages",
                    template:"{common.prev()} {common.pages()} {common.next()}",
                    size:20,
                    group:5
                },
            ]
        }

    }
)