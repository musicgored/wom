define(['util/openModule'],function (openModule) {
    function removeElement(id){
        $$("typeContract").remove(id);
    }

    return {
    type:"space",
    rows: [
        {
            view: 'toolbar',
            id: 'typeContractToolbar',
            cols:[
                {
                    view:'button',
                    label:'Добавить',
                    click:function () {
                        var typeContractList = $$('typeContract')
                        var id = typeContractList.add({

                        })
                        typeContractList.editRow(id)
                    }
                },
            ]
        },
        {
            view: "datatable", id: "typeContract", editable: true,
            pager:"typeContractPages",
            columns: [
                {id: "typeName", header:["Тип договора", {content:"textFilter"}], fillspace: true, editor: "text"},
                {id:'period',editor: "date", header: "Период начала", width: 120, format:webix.Date.dateToStr("%Y-%m-%d")},
                {id:'period_end',editor: "date", header: "Период конец", width: 120, format:webix.Date.dateToStr("%Y-%m-%d")},
                { header:[""], width:50, template:"{common.trashIcon()}" }
            ],
            rules:{
                typeName: webix.rules.isNotEmpty,
                period: webix.rules.isNotEmpty,
                period_end: webix.rules.isNotEmpty,
            },
            scheme:{
                $init: function(){
                },
                $update: function (obj) {
                    obj.period = webix.i18n.parseFormatStr(obj.period).substring(0,10)
                    obj.period_end = webix.i18n.parseFormatStr(obj.period_end).substring(0,10)
                },
                $save: function (obj) {
                    obj.period = webix.i18n.parseFormatStr(obj.period).substring(0,10)
                    obj.period_end = webix.i18n.parseFormatStr(obj.period_end).substring(0,10)

                }
            },
            onClick:{
                "wxi-trash":(e,id) => removeElement(id),
            },
            autoheight: true,
            minHeight: 300,
            scroll: "auto",
            on: {
                onBeforeLoad: function () {
                    this.showOverlay("Загрузка...");
                },
                onAfterLoad: function () {
                    this.hideOverlay();
                    openModule.mod.add($$("typeContract"))
                    openModule.mod.add($$("typeContractPages"))
                    openModule.mod.add($$("typeContractToolbar"))
                }
            },

            url: "resource->api/public/type_contract/",
            save: "resource->api/public/type_contract/"
        },
        {
            view:'pager',
            id:"typeContractPages",
            template:"{common.prev()} {common.pages()} {common.next()}",
            size:20,
            group:5
        },
    ]
}
})