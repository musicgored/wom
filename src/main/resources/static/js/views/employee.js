define(['collections/department','collections/workSchedule','util/openModule'],function (department,workSchedule,openModule) {

        function removeElement(id){
            $$("employee").remove(id);
        }

        return {
            type:"space",
            rows: [
                {
                    view: 'toolbar',
                    id:'employeeToolbar',
                    cols:[
                        {
                            view:'button',
                            label:'Добавить',
                            click:function () {
                                var contactList = $$('employee')
                                var id = contactList.add({

                                })
                                contactList.editRow(id)
                            }
                        },
                    ]
                },
                {
                    view: "datatable",
                    id: "employee",
                    editable: true,
                    pager:"employeePages",
                    columns: [
                        {id: "lastName", header:["Фамилия", {content:"textFilter"}], fillspace: true, editor: "text"},
                        {id: "name", header:["Имя", {content:"textFilter"}], fillspace: true, editor: "text"},
                        {id: "middleName", header:["Отчество", {content:"textFilter"}], fillspace: true, editor: "text"},
                        {id: "department", header: ["Отдел",{content:"selectFilter"}], fillspace: true, collection: department, editor: "combo"},
                        {id: "workSchedule", header: ["График работы",{content:"selectFilter"}], fillspace: true, collection: workSchedule, editor: "combo"},
                        { header:[""], width:50, template:"{common.trashIcon()}" }
                    ],
                    rules:{
                        name: webix.rules.isNotEmpty,
                        lastName: webix.rules.isNotEmpty,
                        middleName: webix.rules.isNotEmpty,
                        department: webix.rules.isNotEmpty,
                    },
                    scheme:{
                        $init: function() {

                        },
                        $update: function (obj) {

                        },
                        $save: function (obj) {

                        }
                    },
                    onClick:{
                        "wxi-trash":(e,id) => removeElement(id),
                    },
                    autoheight: true,
                    minHeight: 300,
                    scroll: "auto",
                    on: {
                        onBeforeLoad: function () {
                            this.showOverlay("Загрузка...");
                        },
                        onAfterLoad: function () {
                            this.hideOverlay();
                            openModule.mod.add($$('employee'))
                            openModule.mod.add($$('employeePages'))
                            openModule.mod.add($$('employeeToolbar'))
                        }
                    },
                    url: "resource->api/public/employee/",
                    save: "resource->api/public/employee/"

                },
                {
                    view:'pager',
                    id:"employeePages",
                    template:"{common.prev()} {common.pages()} {common.next()}",
                    size:20,
                    group:5
                },
            ]
        }

    }
)