define(['util/openModule'],function (openModule) {
    function removeElement(id){
        $$("department").remove(id);
    }


    return {
        type:"space",
        rows: [
            {
                view: 'toolbar',
                id: 'departmentToolbar',
                cols:[
                    {
                        view:'button',
                        label:'Добавить',
                        click:function () {
                            var typeContractList = $$('department')
                            var id = typeContractList.add({

                            })
                            typeContractList.editRow(id)
                        }
                    },
                ]
            },
            {

                view: "datatable", id: "department", editable: true,
                pager:"departmentPages",
                columns: [
                    {id: "nameDepartment", header:["Название отдела", {content:"textFilter"}], fillspace: true, editor: "text"},
                    { header:[""], width:50, template:"{common.trashIcon()}" }
                ],
                rules:{
                    nameDepartment: webix.rules.isNotEmpty,
                },
                scheme:{
                    $init: function(){
                    },
                    $update: function (obj) {

                    },
                    $save: function (obj) {

                    }
                },
                onClick:{
                    "wxi-trash":(e,id) => removeElement(id),
                },
                autoheight: true,
                minHeight: 300,
                scroll: "auto",
                on: {
                    onBeforeLoad: function () {
                        this.showOverlay("Загрузка...");
                    },
                    onAfterLoad: function () {
                        this.hideOverlay();
                        openModule.mod.add($$("department"))
                        openModule.mod.add($$("departmentPages"))
                        openModule.mod.add($$("departmentToolbar"))
                    }
                },

                url: "resource->api/public/department/",
                save: "resource->api/public/department/"
            },
            {
                view:'pager',
                id:"departmentPages",
                template:"{common.prev()} {common.pages()} {common.next()}",
                size:15,
                group:5
            },
        ]
    }
})