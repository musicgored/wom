package ru.khvsoft.WOM.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;
import ru.khvsoft.WOM.dto.ContractListDTO;
import ru.khvsoft.WOM.entity.Contract;
import ru.khvsoft.WOM.entity.Users;
import ru.khvsoft.WOM.exeption_handling.ControllerError;
import ru.khvsoft.WOM.exeption_handling.ControllerException;
import ru.khvsoft.WOM.repo.ContractRepo;
import ru.khvsoft.WOM.service.ContractService;
import ru.khvsoft.WOM.service.UsersService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/api/public/contract/")
@CrossOrigin(origins = "*", maxAge = 3600)
public class ContractController {

    @Autowired
    ContractService contractService;

    @Autowired
    ContractRepo contractRepo;

    @Autowired
    UsersService usersService;

    @GetMapping()
    public List <Contract> getContracts(@AuthenticationPrincipal User user){
        Users users = usersService.getUserByUserName(user.getUsername());
        return contractRepo.getContractWhereCompanyId(users.getSelectedCompany().getId());
    }

    @GetMapping("items")
    public List <ContractListDTO> getContractsItems(@AuthenticationPrincipal User user){
        Users users = usersService.getUserByUserName(user.getUsername());
        return ContractListDTO.toContractListDTO(contractRepo.getContractWhereCompanyId(users.getSelectedCompany().getId()));
    }


    @GetMapping("{id}")
    public Contract getContract(@PathVariable (name = "id") long id) {
        Optional <Contract> contract = contractService.findContract(id);
        if (!contract.isPresent()) {throw new ControllerException("Не найден эелемент  c id = "+id);}
        return contract.get();
    }

    @PostMapping()
    public Contract addContract(@Valid @RequestBody Contract contract,@AuthenticationPrincipal User user){
        Users users = usersService.getUserByUserName(user.getUsername());
        contract.setCompany(users.getSelectedCompany());
        return contractService.addContract(contract);
    }

    @PutMapping()
    public Contract updateContract(@Valid @RequestBody Contract contract,@AuthenticationPrincipal User user){
        Users users = usersService.getUserByUserName(user.getUsername());
        contract.setCompany(users.getSelectedCompany());
        return contractService.addContract(contract);
    }

    @DeleteMapping("{id}")
    public void deleteContract(@PathVariable (name = "id") long id){
        contractService.deleteContract(id);
    }

    @ExceptionHandler
    public ResponseEntity<ControllerError> errorResponseEntity(Exception exception) {
        log.error("/// exeption: "+exception.getMessage()+"/// localized: "+exception.getClass().getName());
        return new ResponseEntity<> (new ControllerError(exception.getMessage(),exception.getClass().getName()), HttpStatus.BAD_REQUEST);
    }
}
