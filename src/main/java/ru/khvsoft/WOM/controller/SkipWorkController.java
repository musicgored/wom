package ru.khvsoft.WOM.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;
import ru.khvsoft.WOM.classes.ReasonSkip;
import ru.khvsoft.WOM.dto.ReasonSkipListDTO;
import ru.khvsoft.WOM.dto.SkipWorkListDTO;
import ru.khvsoft.WOM.entity.SkipWork;
import ru.khvsoft.WOM.entity.Users;
import ru.khvsoft.WOM.exeption_handling.ControllerError;
import ru.khvsoft.WOM.exeption_handling.ControllerException;
import ru.khvsoft.WOM.repo.SkipWorkRepo;
import ru.khvsoft.WOM.service.SkipWorkService;
import ru.khvsoft.WOM.service.UsersService;

import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/api/public/skip_work/")
@CrossOrigin(origins = "*", maxAge = 3600)
public class SkipWorkController {

@Autowired
SkipWorkService skipWorkService;

@Autowired
UsersService usersService;

@Autowired
SkipWorkRepo skipWorkRepo;

@GetMapping()
public List<SkipWork> findAllListSkipWork(){
    return skipWorkService.finAllSkipWork();
}

@GetMapping("items")
public List<SkipWorkListDTO> findAllListSkipWorkItems(@AuthenticationPrincipal User user){
    Users users = usersService.getUserByUserName(user.getUsername());
    return SkipWorkListDTO.toSkipWorkListDTO(skipWorkRepo.selectSkipWorkWhereCompanyId(users.getSelectedCompany().getId()));
}

@GetMapping("reason_skip")
public List<ReasonSkipListDTO> getAllReasonSkip(){
    return ReasonSkipListDTO.toReasonSkipListDTO();
}

@GetMapping("{id}")
public SkipWork findSkipWorkById(@PathVariable(name = "id") long id){
    Optional <SkipWork> skipWork = skipWorkService.findSkipWorkById(id);
    if (!skipWork.isPresent()) {throw new ControllerException("Не найден эелемент  c id = "+id);}
    return skipWork.get();
}

@PostMapping()
public SkipWork addSkipWork(@RequestBody SkipWork skipWork){
    return skipWorkService.addSkipWork(skipWork);
}

@PutMapping()
public SkipWork updateSkipWork(@RequestBody SkipWork skipWork){
    log.debug(skipWork.toString());
    return skipWorkService.addSkipWork(skipWork);
}

@DeleteMapping("{id}")
public boolean deleteSkipWOrk(@PathVariable (name = "id") long id){
    skipWorkService.deleteSkipWorkById(id);
    return true;
}

@GetMapping("where_reasonSkipWork/{skipWorkString}")
public List <SkipWork> selectSkipWorkWhereReasonSkip(@PathVariable(name = "skipWorkString") String skipWorkString){
    ReasonSkip reasonSkip = ReasonSkip.valueOf(skipWorkString);
    return skipWorkService.selectSkipWorkWhereReasonSkip(reasonSkip);
}

@GetMapping("where_reasonSkipWorkAndEmployeeId/{skipWorkString}/{id}")
public List<SkipWork> selectSkipWorkWhereEmployeeIdAndReasonSkip(@PathVariable (name = "skipWorkString") String skipWorkString, @PathVariable (name = "id") long id){
    ReasonSkip reasonSkip = ReasonSkip.valueOf(skipWorkString);
    return skipWorkService.selectSkipWorkWhereEmployeeIdAndReasonSkip(id,reasonSkip);
}

@ExceptionHandler
public ResponseEntity<ControllerError> errorResponseEntity(Exception exception) {
log.error("/// exeption: "+exception.getMessage()+"/// localized: "+exception.getClass().getName());
return new ResponseEntity<> (new ControllerError(exception.getMessage(),exception.getClass().getName()), HttpStatus.BAD_REQUEST);
}

}
