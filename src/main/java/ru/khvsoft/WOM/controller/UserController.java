package ru.khvsoft.WOM.controller;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.khvsoft.WOM.dto.CompanyListDTO;
import ru.khvsoft.WOM.dto.UserInfoDTO;
import ru.khvsoft.WOM.entity.Company;
import ru.khvsoft.WOM.entity.Users;
import ru.khvsoft.WOM.exeption_handling.ControllerError;
import ru.khvsoft.WOM.repo.CompanyRepo;
import ru.khvsoft.WOM.service.UsersService;

import java.util.List;
import java.util.Set;

@Slf4j
@RestController
@RequestMapping("/api/public/user/")
public class UserController {

@Autowired
UsersService usersService;

@Autowired
    CompanyRepo companyRepo;

@GetMapping
public UserInfoDTO getUserInfo(@AuthenticationPrincipal User user){
    Users users = usersService.getUserByUserName(user.getUsername());
    List<Company> companySet = companyRepo.getCompanyWhereUserId(users.getId());
    return new UserInfoDTO(user, CompanyListDTO.getCompanyListDTO(companySet));
}

    @ExceptionHandler
    public ResponseEntity<ControllerError> errorResponseEntity(Exception exception) {
        log.error("/// exeption: "+exception.getMessage()+"/// localized: "+exception.getClass().getName());
        return new ResponseEntity<> (new ControllerError(exception.getMessage(),exception.getClass().getName()), HttpStatus.BAD_REQUEST);
    }

}
