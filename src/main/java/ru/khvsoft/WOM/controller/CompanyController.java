package ru.khvsoft.WOM.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.khvsoft.WOM.dto.CompanyListDTO;
import ru.khvsoft.WOM.entity.Company;
import ru.khvsoft.WOM.entity.Users;
import ru.khvsoft.WOM.repo.CompanyRepo;
import ru.khvsoft.WOM.service.UsersService;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/public/company/")

public class CompanyController {

@Autowired
UsersService usersService;

@Autowired
CompanyRepo companyRepo;

@GetMapping("items")
public List<CompanyListDTO> getCompanyListDTO(@AuthenticationPrincipal User user){
    Users users = usersService.getUserByUserName(user.getUsername());
    List<Company> companySet = companyRepo.getCompanyWhereUserId(users.getId());
    return CompanyListDTO.getCompanyListDTO(companySet);
}

}
