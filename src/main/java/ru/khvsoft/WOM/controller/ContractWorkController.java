package ru.khvsoft.WOM.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;
import ru.khvsoft.WOM.entity.ContractWork;
import ru.khvsoft.WOM.entity.Users;
import ru.khvsoft.WOM.exeption_handling.ControllerError;
import ru.khvsoft.WOM.exeption_handling.ControllerException;
import ru.khvsoft.WOM.repo.ContractWorkRepo;
import ru.khvsoft.WOM.service.ContractWorkService;
import ru.khvsoft.WOM.service.UsersService;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/api/public/contract_work/")
@CrossOrigin(origins = "*", maxAge = 3600)
public class ContractWorkController {

    @Autowired
    ContractWorkService contractWorkService;

    @Autowired
    ContractWorkRepo contractWorkRepo;

    @Autowired
    UsersService usersService;

    @GetMapping()
    public List <ContractWork> getContractWorks(@AuthenticationPrincipal User user){
        Users users = usersService.getUserByUserName(user.getUsername());
        return contractWorkRepo.selectContractWordWhereCompanyId(users.getSelectedCompany().getId());
    }

    @GetMapping("{id}")
    public ContractWork getContractWork(@PathVariable (name = "id") long id) {
        Optional <ContractWork> contract = contractWorkService.findContractWorkById(id);
        if (!contract.isPresent()) {throw new ControllerException("Не найден эелемент  c id = "+id);}
        return contract.get();
    }

    @PostMapping()
    public ContractWork addContractWork(@Valid @RequestBody ContractWork contractWork,@AuthenticationPrincipal User user){
        Users users = usersService.getUserByUserName(user.getUsername());
        return contractWorkService.addContractWork(contractWork);
    }

    @PutMapping()
    public ContractWork updateContractWork(@Valid @RequestBody ContractWork contractWork,@AuthenticationPrincipal User user){
        return contractWorkService.addContractWork(contractWork);
    }

    @DeleteMapping("{id}")
    public void deleteContractWork(@PathVariable (name = "id") long id){
        contractWorkService.deleteContractWorkById(id);
    }

    @GetMapping("get_where_emp_id/{id}")
    public List<ContractWork> selectContactWorkWhereEmployeeId(@PathVariable (name = "id") long id){
        return contractWorkService.selectContactWorkWhereEmployeeId(id);
    }

    @GetMapping("get_where_emp_id_and_date/{id}/{dateWorkString}")
    public List<ContractWork> selectContractWorkWhereEmployeeIdAndDateWork(@PathVariable(name = "id") long id, @PathVariable(name = "dateWorkString") String dateWorkString){
        LocalDate dateWork = LocalDate.parse(dateWorkString);
        return contractWorkService.selectContractWorkWhereEmployeeIdAndDateWork(id,dateWork);
    }

    @GetMapping("get_if_between_start_date_and_end_date/{startDateString}/{endDateString}")
    public List<ContractWork> selectContractWorkBetweenStartDateAndEndDate(@PathVariable(name = "startDateString") String startDateString, @PathVariable(name = "endDateString") String endDateString){
        LocalDate startDate = LocalDate.parse(startDateString);
        LocalDate endDate = LocalDate.parse(endDateString);
        if(startDate.compareTo(endDate)>0) {throw new ControllerException("Начальная дата позже второй");}
        return contractWorkService.selectContractWorkBetweenStartDateAndEndDate(startDate,endDate);
    }

    @ExceptionHandler
    public ResponseEntity<ControllerError> errorResponseEntity(Exception exception) {
        log.error("/// exeption: "+exception.getMessage()+"/// localized: "+exception.getClass().getName());
        return new ResponseEntity<> (new ControllerError(exception.getMessage(),exception.getClass().getName()), HttpStatus.BAD_REQUEST);
    }
}
