package ru.khvsoft.WOM.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;
import ru.khvsoft.WOM.dto.DepartmentsListDTO;
import ru.khvsoft.WOM.entity.Department;
import ru.khvsoft.WOM.entity.TypeContract;
import ru.khvsoft.WOM.entity.Users;
import ru.khvsoft.WOM.exeption_handling.ControllerError;
import ru.khvsoft.WOM.exeption_handling.ControllerException;
import ru.khvsoft.WOM.repo.DepartmentRepo;
import ru.khvsoft.WOM.service.DepartmentService;
import ru.khvsoft.WOM.service.TypeContractService;
import ru.khvsoft.WOM.service.UsersService;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/api/public/department/")
@CrossOrigin(origins = "*", maxAge = 3600)
public class DepartmentController {

    @Autowired
    DepartmentService departmentService;

    @Autowired
    UsersService usersService;

    @Autowired
    DepartmentRepo departmentRepo;

    @GetMapping()
    public List <Department> getDepartments(@AuthenticationPrincipal User user){
        Users users = usersService.getUserByUserName(user.getUsername());
        return departmentRepo.getDepartmentWhereCompanyId(users.getSelectedCompany().getId());
    }

    @GetMapping("items")
    public List <DepartmentsListDTO> getDepartmentsItems(@AuthenticationPrincipal User user){
        Users users = usersService.getUserByUserName(user.getUsername());
        return DepartmentsListDTO.getDepartmentListDTO(departmentRepo.getDepartmentWhereCompanyId(users.getSelectedCompany().getId()));
    }

    @GetMapping("{id}")
    public Department getDepartment(@PathVariable (name = "id") long id) {
        Optional <Department> contract = departmentService.findDepartmentById(id);
        if (!contract.isPresent()) {throw new ControllerException("Не найден эелемент  c id = "+id);}
        return contract.get();
    }

    @PostMapping()
    public Department addDepartment(@Valid @RequestBody Department department,@AuthenticationPrincipal User user){
        Users users = usersService.getUserByUserName(user.getUsername());
        department.setCompany(users.getSelectedCompany());
        return departmentService.addDepartment(department);
    }


    @PutMapping()
    public Department updateDepartment(@Valid @RequestBody Department department,@AuthenticationPrincipal User user){
        Users users = usersService.getUserByUserName(user.getUsername());
        department.setCompany(users.getSelectedCompany());
        return departmentService.addDepartment(department);
    }

    @DeleteMapping("{id}")
    public void deleteDepartment(@PathVariable (name = "id") long id){
        departmentService.deleteDepartmentById(id);
    }

    @ExceptionHandler
    public ResponseEntity<ControllerError> errorResponseEntity(Exception exception) {
        log.error("/// exeption: "+exception.getMessage()+"/// localized: "+exception.getClass().getName());
        return new ResponseEntity<> (new ControllerError(exception.getMessage(),exception.getClass().getName()), HttpStatus.BAD_REQUEST);
    }
}
