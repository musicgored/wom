package ru.khvsoft.WOM.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.khvsoft.WOM.entity.Users;
import ru.khvsoft.WOM.exeption_handling.ControllerError;
import ru.khvsoft.WOM.exeption_handling.ControllerException;
import ru.khvsoft.WOM.service.UsersService;
import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/api/service")
@CrossOrigin(origins = "*", maxAge = 3600)
public class ServiceController {

@Autowired
UsersService usersService;

@PostMapping("/user/")
public boolean addUser(@RequestBody Users user){
return usersService.addUser(user);
}

@PutMapping("/user/")
public boolean updateUser(@RequestBody Users user){
return usersService.updateUser(user);
}

@DeleteMapping("/user/{id}")
public boolean deleteUser(@PathVariable(name = "id") long id){
    usersService.deleteUser(id);
    return true;
}

@GetMapping("/user/")
public List<Users> getAllUsers(){
    return usersService.getAllUsers();
}

@GetMapping("/user/{id}")
public Users getUser(@PathVariable (name = "id") long id) {
    Optional <Users> user = usersService.getUserById(id);
    if (!user.isPresent()) {throw new ControllerException("Не найден эелемент c id = "+id);}
    return user.get();
}

@ExceptionHandler
public ResponseEntity<ControllerError> errorResponseEntity(Exception exception) {
log.error("/// exeption: "+exception.getMessage()+"/// localized: "+exception.getClass().getName());
return new ResponseEntity<> (new ControllerError(exception.getMessage(),exception.getClass().getName()), HttpStatus.BAD_REQUEST);
}

}
