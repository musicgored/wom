package ru.khvsoft.WOM.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;
import ru.khvsoft.WOM.dto.EmployeeListDTO;
import ru.khvsoft.WOM.dto.EmployeeWorkListDTO;
import ru.khvsoft.WOM.dto.Reports.WorkEmployeeReportsDTO;
import ru.khvsoft.WOM.entity.ContractWork;
import ru.khvsoft.WOM.entity.Employee;
import ru.khvsoft.WOM.entity.SkipWork;
import ru.khvsoft.WOM.entity.Users;
import ru.khvsoft.WOM.exeption_handling.ControllerError;
import ru.khvsoft.WOM.exeption_handling.ControllerException;
import ru.khvsoft.WOM.repo.EmployeeRepo;
import ru.khvsoft.WOM.repo.SkipWorkRepo;
import ru.khvsoft.WOM.service.EmployeeService;
import ru.khvsoft.WOM.service.UsersService;
import ru.khvsoft.WOM.service.WorkEmployeeReportsDTOService;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/api/public/employee/")
@CrossOrigin(origins = "*", maxAge = 3600)
public class EmployeeController {
    @Autowired
    EmployeeService employeeService;

    @Autowired
    EmployeeRepo employeeRepo;

    @Autowired
    UsersService usersService;

    @Autowired
    WorkEmployeeReportsDTOService workEmployeeReportsDTOService;

    @Autowired
    SkipWorkRepo skipWorkRepo;

    @GetMapping()
    public List <Employee> getEmployees(@AuthenticationPrincipal User user){
        Users users = usersService.getUserByUserName(user.getUsername());
        return employeeRepo.getEmployeeWhereCompanyId(users.getSelectedCompany().getId());
    }

    @GetMapping("contract_work/{data}")
    public List <EmployeeWorkListDTO> getEmployeesWereContractWorkData(@AuthenticationPrincipal User user, @PathVariable(name = "data") String data){
        Users users = usersService.getUserByUserName(user.getUsername());
        return employeeService.getEmployeeWhereDataContractWork(LocalDate.parse(data),users.getSelectedCompany().getId());
    }

    @PostMapping ("contract_work/{data}")
    public ContractWork setEmployeesWereContractWorkData(@AuthenticationPrincipal User user, @PathVariable(name = "data") String data, @RequestBody EmployeeWorkListDTO employeeWorkListDTO){
        return employeeService.toContractWorkFromEmployeeWorkListDTO(employeeWorkListDTO,LocalDate.parse(data));
    }

    @PutMapping ("contract_work/{data}")
    public void setEmployeesWereContractWorkDataType(@AuthenticationPrincipal User user, @PathVariable(name = "data") String data){
        Users users = usersService.getUserByUserName(user.getUsername());
        employeeService.setContractWorkCopyFromLastContractWork(LocalDate.parse(data),users.getSelectedCompany().getId());
    }

    @Transactional
    @DeleteMapping ("contract_work/{data}")
    public void deleteEmployeesWereContractWorkData(@AuthenticationPrincipal User user, @PathVariable(name = "data") String data){
        Users users = usersService.getUserByUserName(user.getUsername());
        skipWorkRepo.deleteSkipWorkWhereDateAndCompanyId(LocalDate.parse(data));
        employeeService.deleteEmployeeWhereDataContractWork(LocalDate.parse(data));
    }

    @PutMapping ("contract_work/{data1}/{data2}")
    public void updateEmployeesWereContractWorkData(@AuthenticationPrincipal User user, @PathVariable(name = "data1") String data1, @PathVariable(name = "data2") String data2){
        Users users = usersService.getUserByUserName(user.getUsername());
        employeeService.toCopyContractWorkFromEmployeeWorkListDTO(LocalDate.parse(data1),LocalDate.parse(data2));
    }

    @GetMapping ("reports/{startDate}/{endDate}")
    public List<WorkEmployeeReportsDTO> getReportsEmployeesWereContractWorkAndSkipWorkStartDateEndDate(@AuthenticationPrincipal User user, @PathVariable(name = "startDate") String startDate, @PathVariable(name = "endDate") String endDate){
        Users users = usersService.getUserByUserName(user.getUsername());
        return workEmployeeReportsDTOService.getWorkEmployeeReportsDTO(LocalDate.parse(startDate),LocalDate.parse(endDate),users.getSelectedCompany().getId());
    }

    @GetMapping("items")
    public List <EmployeeListDTO> getEmployeesItems(@AuthenticationPrincipal User user){
        Users users = usersService.getUserByUserName(user.getUsername());
        return EmployeeListDTO.toEmployeeListDTO(employeeRepo.getEmployeeWhereCompanyId(users.getSelectedCompany().getId()));
    }

    @GetMapping("{id}")
    public Employee getEmployee(@PathVariable (name = "id") long id) {
        Optional <Employee> employee = employeeService.getEmployee(id);
        if (!employee.isPresent()) {throw new ControllerException("Не найден эелемент  c id = "+id);}
        return employee.get();
    }

    @PostMapping()
    public Employee addEmployee(@Valid @RequestBody Employee employee,@AuthenticationPrincipal User user){
        return employeeService.addEmployee(employee);
    }

    @PutMapping()
    public Employee updateEmployee(@Valid @RequestBody Employee employee,@AuthenticationPrincipal User user){
        return employeeService.addEmployee(employee);
    }

    @DeleteMapping("{id}")
    public void deleteEmployee(@PathVariable (name = "id") long id){
        employeeService.deleteEmployee(id);
    }

    @ExceptionHandler
    public ResponseEntity<ControllerError> errorResponseEntity(Exception exception) {
        log.error("/// exeption: "+exception.getMessage()+"/// localized: "+exception.getClass().getName());
        return new ResponseEntity<> (new ControllerError(exception.getMessage(),exception.getClass().getName()), HttpStatus.BAD_REQUEST);
    }
}
