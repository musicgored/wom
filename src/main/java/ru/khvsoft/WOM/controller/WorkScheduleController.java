package ru.khvsoft.WOM.controller;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;
import ru.khvsoft.WOM.dto.WorkScheduleListDTO;
import ru.khvsoft.WOM.entity.Users;
import ru.khvsoft.WOM.entity.WorkSchedule;
import ru.khvsoft.WOM.exeption_handling.ControllerError;
import ru.khvsoft.WOM.repo.WorkScheduleRepo;
import ru.khvsoft.WOM.service.UsersService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/public/work_schedule/")
@Slf4j
public class WorkScheduleController {

    @Autowired
    UsersService usersService;

    @Autowired
    WorkScheduleRepo workScheduleRepo;


    @GetMapping
    public List<WorkSchedule> getAllWorkSchedule(@AuthenticationPrincipal User user){
        Users users = usersService.getUserByUserName(user.getUsername());
        return workScheduleRepo.findAll().stream().filter(wh->wh.getCompany().equals(users.getSelectedCompany())).collect(Collectors.toList());
    }

    @GetMapping("items")
    public List<WorkScheduleListDTO> getAllWorkScheduleItems(@AuthenticationPrincipal User user){
        Users users = usersService.getUserByUserName(user.getUsername());
        return WorkScheduleListDTO.toWorkScheduleListDTOList(workScheduleRepo.findAll().stream().filter(wh->wh.getCompany().equals(users.getSelectedCompany())).collect(Collectors.toList()));
    }


    @PostMapping
    public WorkSchedule addWorkSchedule(@RequestBody WorkSchedule workSchedule,@AuthenticationPrincipal User user){
        Users users = usersService.getUserByUserName(user.getUsername());
        workSchedule.setCompany(users.getSelectedCompany());
        return workScheduleRepo.save(workSchedule);
    }

    @PutMapping
    public WorkSchedule updateWorkSchedule(@RequestBody WorkSchedule workSchedule,@AuthenticationPrincipal User user){
        Users users = usersService.getUserByUserName(user.getUsername());
        workSchedule.setCompany(users.getSelectedCompany());
        return workScheduleRepo.save(workSchedule);
    }

    @DeleteMapping("{id}")
    public void deleteWorkSchedule(@PathVariable (name = "id") long id) {
        workScheduleRepo.deleteById(id);
    }

    @ExceptionHandler
    public ResponseEntity<ControllerError> errorResponseEntity(Exception exception) {
        log.error("/// exeption: "+exception.getMessage()+"/// localized: "+exception.getClass().getName());
        return new ResponseEntity<> (new ControllerError(exception.getMessage(),exception.getClass().getName()), HttpStatus.BAD_REQUEST);
    }
}
