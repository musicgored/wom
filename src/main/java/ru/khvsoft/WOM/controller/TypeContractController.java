package ru.khvsoft.WOM.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;
import ru.khvsoft.WOM.dto.TypeContractListItemDTO;
import ru.khvsoft.WOM.entity.Company;
import ru.khvsoft.WOM.entity.Contract;
import ru.khvsoft.WOM.entity.TypeContract;
import ru.khvsoft.WOM.entity.Users;
import ru.khvsoft.WOM.exeption_handling.ControllerError;
import ru.khvsoft.WOM.exeption_handling.ControllerException;
import ru.khvsoft.WOM.service.ContractService;
import ru.khvsoft.WOM.service.TypeContractService;
import ru.khvsoft.WOM.service.UsersService;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/api/public/type_contract/")
@CrossOrigin(origins = "*", maxAge = 3600)
public class TypeContractController {

    @Autowired
    TypeContractService typeContractService;

    @Autowired
    ContractService contractService;

    @Autowired
    UsersService usersService;

    @GetMapping()
    public List <TypeContract> getTypeContracts(@AuthenticationPrincipal User user){
        return typeContractService.getTypeContractWhereCompanySelected(user.getUsername());
    }

    @GetMapping("company/{companyId}")
    public List <TypeContract> getTypeContractsWhereCompanysId(@PathVariable(name = "companyId") long companyId){
        return typeContractService.getTypeContractWhereCompanyId(companyId);
    }

    @GetMapping("items")
    public List<TypeContractListItemDTO> getTypeContractsItems(@AuthenticationPrincipal User user){
        return TypeContractListItemDTO.getTypeContractListItemDTO(typeContractService.getTypeContractWhereCompanySelected(user.getUsername()));
    }

    @GetMapping("{id}")
    public TypeContract getTypeContract(@PathVariable (name = "id") long id) {
        Optional <TypeContract> contract = typeContractService.findByIdTypeContract(id);
        if (!contract.isPresent()) {throw new ControllerException("Не найден эелемент  c id = "+id);}
        return contract.get();
    }

    @PostMapping()
    public TypeContract addTypeContract(@Valid @RequestBody TypeContract typecontract,@AuthenticationPrincipal User user){
        Users users = usersService.getUserByUserName(user.getUsername());
        typecontract.setCompany(users.getSelectedCompany());
        return typeContractService.addTypeContract(typecontract);
    }

    @PutMapping()
    public TypeContract updateTypeContract(@Valid @RequestBody TypeContract typecontract,@AuthenticationPrincipal User user){
        Users users = usersService.getUserByUserName(user.getUsername());
        typecontract.setCompany(users.getSelectedCompany());
        return typeContractService.addTypeContract(typecontract);
    }

    @DeleteMapping("{id}")
    public void deleteTypeContract(@PathVariable (name = "id") long id){
        try {typeContractService.deleteTypeContractById(id);} catch (Exception e){
            throw  new ControllerException(e.getMessage());
        }
    }

    @ExceptionHandler
    public ResponseEntity<ControllerError> errorResponseEntity(Exception exception) {
        log.error("/// exeption: "+exception.getMessage()+"/// localized: "+exception.getClass().getName());
        return new ResponseEntity<> (new ControllerError(exception.getMessage(),exception.getClass().getName()), HttpStatus.BAD_REQUEST);
    }
}
