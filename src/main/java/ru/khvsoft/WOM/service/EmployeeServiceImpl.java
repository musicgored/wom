package ru.khvsoft.WOM.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.khvsoft.WOM.classes.ReasonSkip;
import ru.khvsoft.WOM.dto.EmployeeWorkListDTO;
import ru.khvsoft.WOM.entity.ContractWork;
import ru.khvsoft.WOM.entity.Employee;
import ru.khvsoft.WOM.entity.SkipWork;
import ru.khvsoft.WOM.repo.ContractWorkRepo;
import ru.khvsoft.WOM.repo.EmployeeRepo;
import ru.khvsoft.WOM.repo.SkipWorkRepo;

import javax.transaction.Transactional;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    @Autowired
    EmployeeRepo employeeRepo;

    @Autowired
    ContractWorkRepo contractWorkRepo;

    @Autowired
    SkipWorkRepo skipWorkRepo;

    @Autowired
    SkipWorkService skipWorkService;

    @Override
    public List<Employee> getAllEmployee() {
        return employeeRepo.findAll();
    }

    @Override
    public Employee addEmployee(Employee employee) {
        return employeeRepo.save(employee);
    }

    @Override
    public Optional<Employee> getEmployee(long id) {
        return employeeRepo.findById(id);
    }

    @Override
    public void deleteEmployee(long id){
        employeeRepo.deleteById(id);
    }

    @Transactional
    @Override
    public List<EmployeeWorkListDTO> getEmployeeWhereDataContractWork(LocalDate dateWork, long companyId) {
        List<ContractWork> contractWorkList = contractWorkRepo.selectContractWorkWhereDateWork(dateWork);
        List<Employee> employeeList = employeeRepo.getEmployeeWhereCompanyId(companyId);
        List<SkipWork> skipWorkList = skipWorkRepo.selectSkipWorkWhereSkipDate(dateWork);
        List <EmployeeWorkListDTO> employeeWorkListDTO = new ArrayList<>();
        for (Employee emp:employeeList) {
            EmployeeWorkListDTO employeeWorkListDTO1 = new EmployeeWorkListDTO(emp);
            Optional<ContractWork> contractWork =  contractWorkList.stream().filter(o -> o.getEmployee().equals(emp)).findFirst();
            Optional<SkipWork> skipWork = skipWorkList.stream().filter(o -> o.getEmployee().equals(emp)).findFirst();
            if (contractWork.isPresent()) {
                employeeWorkListDTO1.setContract(contractWork.get().getContract());
            }
            if (skipWork.isPresent()){
                employeeWorkListDTO1.setReasonSkip(skipWork.get().getReasonSkip());
            }
            employeeWorkListDTO.add(employeeWorkListDTO1);
        }
        return employeeWorkListDTO;
    }

    @Transactional
    @Override
    public ContractWork toContractWorkFromEmployeeWorkListDTO(EmployeeWorkListDTO employeeWorkListDTO, LocalDate dateWork) {
        List<ContractWork> contractWorkList = contractWorkRepo.selectContractWorkWhereEmployeeIdAndDateWork(employeeWorkListDTO.getEmployee().getId(),dateWork);
        if (!contractWorkList.isEmpty()){
            ContractWork contractWorkNew = new ContractWork(dateWork,employeeWorkListDTO.getEmployee(),employeeWorkListDTO.getContract());
            contractWorkNew.setId(contractWorkList.stream().findFirst().get().getId());
            return contractWorkRepo.save(contractWorkNew);
        };
        return contractWorkRepo.save(new ContractWork(dateWork,employeeWorkListDTO.getEmployee(),employeeWorkListDTO.getContract()));
    }

    @Transactional
    @Override
    public void toCopyContractWorkFromEmployeeWorkListDTO(LocalDate dateWork1, LocalDate dateWork2) {
        List<ContractWork> contractWorkListDate1 = contractWorkRepo.selectContractWorkWhereDateWork(dateWork2);
        List<ContractWork> contractWorkListDate2 = contractWorkRepo.selectContractWorkWhereDateWork(dateWork1);
        for (ContractWork contractWork:contractWorkListDate1) {
            if (contractWorkListDate2.stream().filter(cw -> cw.getEmployee().equals(contractWork.getEmployee())).findFirst().isPresent()){

            } else {
                ContractWork contractWorkNew = new  ContractWork(dateWork1,contractWork.getEmployee(),contractWork.getContract());
                contractWorkRepo.save(contractWorkNew);
                System.out.println(contractWorkNew);
            }
        }
    }

    @Transactional
    @Override
    public void deleteEmployeeWhereDataContractWork(LocalDate dateWork) {
        contractWorkRepo.deleteEmployeeWhereDataContractWork(dateWork);
    }

    @Transactional
    @Override
    public void setContractWorkCopyFromLastContractWork(LocalDate data, long company_id) {
        List<Employee> employeeList = employeeRepo.getEmployeeWhereCompanyId(company_id);
        addWeekend(employeeList,data);
        addWork(employeeList,data);
    }

    public static boolean isWeekend(LocalDate date){
        return (date.getDayOfWeek().toString().equals("SATURDAY")||date.getDayOfWeek().toString().equals("SUNDAY"));
    }

    public boolean isDayIsWork(LocalDate date, Employee employee){
        if (!contractWorkRepo.selectContractWorkWhereDateAndEmployee(date,employee.getId()).isEmpty()) {
            return true;
        }
        return false;
    }

    public boolean isDayIsWeekend(LocalDate date, Employee employee) {
        if (!skipWorkRepo.selectSkipWorkWhereDateAndEmployee(date,employee.getId()).isEmpty()){
            return true;
        } else return false;
    }

    public void addWeekend(List<Employee> employeeList, LocalDate date){
        for (Employee employee:employeeList) {
            if (employee.getWorkSchedule().isWork_weekend()) {
                int qWork = employee.getWorkSchedule().getQuantity_work();
                int qWeekend = employee.getWorkSchedule().getQuantity_weekend();
                int cWork = 0;
                int cWeekend = 0;
                LocalDate startDate = date.minusDays(qWork-qWeekend);
                LocalDate endDate = date.minusDays(1);
                for (int i = 0; i < qWork+qWeekend; i++) {
                    LocalDate day = endDate.minusDays(i);
                    if (isDayIsWork(day,employee)) {cWork++;}
                    if (isDayIsWeekend(day,employee)) {cWeekend++;}
                }
                if (cWeekend<qWeekend||(cWeekend==qWeekend&&!contractWorkRepo.selectContractWorkWhereEmployeeIdAndDateWork(employee.getId(),date.minusDays(2)).isEmpty())) {
                    skipWorkRepo.save(new SkipWork(ReasonSkip.WEEKEND,employee,date,date));
                }
            } else {
                if (date.getDayOfWeek() == DayOfWeek.SUNDAY || date.getDayOfWeek() == DayOfWeek.SATURDAY) {
                    skipWorkRepo.save(new SkipWork(ReasonSkip.WEEKEND,employee,date,date));
                }
            }
        }
    }

    public void addWork(List<Employee> employeeList,LocalDate date){
        for (Employee employee:employeeList) {
            if (skipWorkRepo.selectSkipWorkWhereSkipDateWhereEmployeeId(date,date,employee.getId()).isEmpty()) {
                int qWork = employee.getWorkSchedule().getQuantity_work();
                int qWeekend = employee.getWorkSchedule().getQuantity_weekend();
                LocalDate startDate = date.minusDays(qWork-qWeekend);
                LocalDate endDate = date.minusDays(1);
                Optional <ContractWork> contractWorkNew = contractWorkRepo.selectContractWorkWhereDateWorkAndEmployee(employee.getId(),startDate,endDate).stream().findFirst();
                if (contractWorkNew.isPresent()) {
                    ContractWork contractWorkNew2 = contractWorkNew.get();
                    contractWorkNew2.setDateWork(date);
                    contractWorkRepo.save(contractWorkNew2);
                }
            }
        }
    }

}
