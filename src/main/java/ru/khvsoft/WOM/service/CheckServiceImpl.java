package ru.khvsoft.WOM.service;

import org.springframework.stereotype.Service;

@Service
public class CheckServiceImpl implements CheckService{
    @Override
    public boolean checkDate(String date) {
        return date.matches("([1-2][0-9][0-9][0-9])-(([0][0-9])|([1][0-2]))-(([0-2][1-9])|([3][0-1]))");
    }
}
