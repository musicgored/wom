package ru.khvsoft.WOM.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.khvsoft.WOM.classes.ReasonSkip;
import ru.khvsoft.WOM.dto.Reports.WorkEmployeeReportsDTO;
import ru.khvsoft.WOM.entity.ContractWork;
import ru.khvsoft.WOM.entity.Employee;
import ru.khvsoft.WOM.entity.SkipWork;
import ru.khvsoft.WOM.repo.ContractWorkRepo;
import ru.khvsoft.WOM.repo.EmployeeRepo;
import ru.khvsoft.WOM.repo.SkipWorkRepo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class WorkEmployeeReportsDTOServiceImpl implements WorkEmployeeReportsDTOService{

    @Autowired
    EmployeeRepo employeeRepo;

    @Autowired
    SkipWorkRepo skipWorkRepo;

    @Autowired
    ContractWorkRepo contractWorkRepo;

    @Override
    public List<WorkEmployeeReportsDTO> getWorkEmployeeReportsDTO(LocalDate startDate, LocalDate endDate, long company_id) {
        List<Employee> employeeList = employeeRepo.getEmployeeWhereCompanyId(company_id);
        List<SkipWork> skipWorkList = skipWorkRepo.selectSkipWorkWhereCompanyId(company_id);
        List<ContractWork> contractWorkList = contractWorkRepo.selectContractWordWhereCompanyId(company_id);
        List<WorkEmployeeReportsDTO> workEmployeeReportsDTOList = new ArrayList<>();
        for (Employee employee:employeeList) {
            WorkEmployeeReportsDTO workEmployeeReportsDTO = new WorkEmployeeReportsDTO();
            workEmployeeReportsDTO.setEmployee(employee);
            workEmployeeReportsDTO.setCountMedical(skipWorkList.stream().filter(s->s.getEmployee().equals(employee)).filter(s->(s.getReasonSkip()==ReasonSkip.MEDICAL)).filter(s->s.getStartDate().isAfter(startDate)).filter(s->s.getEndDate().isBefore(endDate)).count());
            workEmployeeReportsDTO.setCountVacation(skipWorkList.stream().filter(s->s.getEmployee().equals(employee)).filter(s->s.getReasonSkip()==ReasonSkip.VACATION).filter(s->s.getStartDate().isAfter(startDate)).filter(s->s.getEndDate().isBefore(endDate)).count());
            workEmployeeReportsDTO.setCountWeekend(skipWorkList.stream().filter(s->s.getEmployee().equals(employee)).filter(s->s.getReasonSkip()==ReasonSkip.WEEKEND).filter(s->s.getStartDate().isAfter(startDate)).filter(s->s.getEndDate().isBefore(endDate)).count());
            workEmployeeReportsDTO.setNoReason(skipWorkList.stream().filter(s->s.getEmployee().equals(employee)).filter(s->s.getReasonSkip()==ReasonSkip.NOREASON).filter(s->s.getStartDate().isAfter(startDate)).filter(s->s.getEndDate().isBefore(endDate)).count());
            workEmployeeReportsDTO.setCountWork(contractWorkList.stream().filter(s->s.getEmployee().equals(employee)).filter(s->s.getDateWork().isAfter(startDate)).filter(s->s.getDateWork().isBefore(endDate)).count());
            workEmployeeReportsDTOList.add(workEmployeeReportsDTO);
        }
        return workEmployeeReportsDTOList;
    }
}
