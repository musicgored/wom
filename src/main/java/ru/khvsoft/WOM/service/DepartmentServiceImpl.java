package ru.khvsoft.WOM.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.khvsoft.WOM.entity.Department;
import ru.khvsoft.WOM.repo.DepartmentRepo;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class DepartmentServiceImpl implements DepartmentService{

    @Autowired
    DepartmentRepo departmentRepo;

    @Override
    public Department addDepartment(Department department) {
        return departmentRepo.save(department);
    }

    @Override
    public List<Department> getDepartment() {
        return departmentRepo.findAll();
    }

    @Override
    public Optional<Department> findDepartmentById(long id) {
        return departmentRepo.findById(id);
    }

    @Override
    public void deleteDepartmentById(long id) {
        departmentRepo.deleteById(id);
    }
}
