package ru.khvsoft.WOM.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.khvsoft.WOM.entity.ContractWork;
import ru.khvsoft.WOM.repo.ContractWorkRepo;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class ContractWorkServiceImpl implements ContractWorkService{

    @Autowired
    ContractWorkRepo contractWorkRepo;

    @Override
    public ContractWork addContractWork(ContractWork contractWork) {
        return contractWorkRepo.save(contractWork);
    }

    @Override
    public Optional<ContractWork> findContractWorkById(long id) {
        return contractWorkRepo.findById(id);
    }

    @Override
    public List<ContractWork> findContractWorkAll() {
        return contractWorkRepo.findAll();
    }

    @Override
    public void deleteContractWorkById(long id) {
        contractWorkRepo.deleteById(id);
    }

    @Override
    public List<ContractWork> selectContactWorkWhereEmployeeId(long id) {
        return contractWorkRepo.selectContactWorkWhereEmployeeId(id);
    }

    @Override
    public List<ContractWork> selectContractWorkWhereEmployeeIdAndDateWork(long id, LocalDate dateWork) {
        return contractWorkRepo.selectContractWorkWhereEmployeeIdAndDateWork(id,dateWork);
    }

    @Override
    public List<ContractWork> selectContractWorkBetweenStartDateAndEndDate(LocalDate startDate, LocalDate endDate) {
        return contractWorkRepo.selectContractWorkBetweenStartDateAndEndDate(startDate,endDate);
    }


}
