package ru.khvsoft.WOM.service;

import ru.khvsoft.WOM.entity.Contract;

import java.util.List;
import java.util.Optional;

public interface ContractService {
Contract addContract(Contract contract);
Optional <Contract> findContract(long id);
void deleteContract(long id);
List<Contract> findAllContract();
}
