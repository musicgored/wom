package ru.khvsoft.WOM.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.khvsoft.WOM.classes.ReasonSkip;
import ru.khvsoft.WOM.entity.SkipWork;
import ru.khvsoft.WOM.repo.SkipWorkRepo;

import java.util.List;
import java.util.Optional;

@Service
public class SkipWorkServiceImpl implements SkipWorkService{

    @Autowired
    SkipWorkRepo skipWorkRepo;

    @Override
    public SkipWork addSkipWork(SkipWork skipWork) {
        return skipWorkRepo.save(skipWork);
    }

    @Override
    public List<SkipWork> finAllSkipWork() {
        return skipWorkRepo.findAll();
    }

    @Override
    public Optional<SkipWork> findSkipWorkById(long id) {
        return skipWorkRepo.findById(id);
    }

    @Override
    public void deleteSkipWorkById(long id) {
        skipWorkRepo.deleteById(id);
    }

    @Override
    public List<SkipWork> selectSkipWorkWhereReasonSkip(ReasonSkip reasonSkip) {
        return skipWorkRepo.selectSkipWorkWhereReasonSkip(reasonSkip);
    }

    @Override
    public List<SkipWork> selectSkipWorkWhereEmployeeIdAndReasonSkip(long id, ReasonSkip reasonSkip) {
        return skipWorkRepo.selectSkipWorkWhereEmployeeIdAndReasonSkip(id,reasonSkip);
    }

    @Override
    public void addSkipWorkIFNotEmpty(SkipWork skipWork) {
        System.out.println(skipWork);
        if (skipWorkRepo.findSkipWorkWhereEmployeeAndSkipWorkDate(skipWork.getEmployee().getId(),skipWork.getStartDate()).isEmpty()){
            skipWorkRepo.save(skipWork);
        };
    }


}
