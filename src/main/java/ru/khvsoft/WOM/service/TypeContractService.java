package ru.khvsoft.WOM.service;

import org.springframework.security.core.userdetails.User;
import ru.khvsoft.WOM.entity.TypeContract;

import java.util.List;
import java.util.Optional;

public interface TypeContractService {
TypeContract addTypeContract(TypeContract typeContract);
List<TypeContract> findAllTypeContract();
Optional<TypeContract> findByIdTypeContract(long id);
void deleteTypeContractById(long id);
List<TypeContract> getTypeContractWhereCompanyId(long companyId);
List<TypeContract> getTypeContractWhereCompanySelected(String userName);
}
