package ru.khvsoft.WOM.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import ru.khvsoft.WOM.entity.TypeContract;
import ru.khvsoft.WOM.repo.ContractRepo;
import ru.khvsoft.WOM.repo.TypeContractRepo;

import java.util.List;
import java.util.Optional;

@Service
public class TypeContractServiceImpl implements TypeContractService{

    @Autowired
    TypeContractRepo typeContractRepo;

    @Autowired
    ContractRepo contractRepo;

    @Override
    public TypeContract addTypeContract(TypeContract typeContract) {
        return typeContractRepo.save(typeContract);
    }

    @Override
    public List<TypeContract> findAllTypeContract() {
        return typeContractRepo.findAll();
    }

    @Override
    public Optional<TypeContract> findByIdTypeContract(long id) {
        return typeContractRepo.findById(id);
    }

    @Override
    public void deleteTypeContractById(long id) {
        typeContractRepo.deleteById(id);
    }

    @Override
    public List<TypeContract> getTypeContractWhereCompanyId(long companyId) {
        return contractRepo.getTypeContractWhereCompanyId(companyId);
    }

    @Override
    public List<TypeContract> getTypeContractWhereCompanySelected(String userName) {
        return typeContractRepo.getTypeContractWhereCompanySelected(userName);
    }
}
