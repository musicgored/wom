package ru.khvsoft.WOM.service;

import ru.khvsoft.WOM.dto.EmployeeWorkListDTO;
import ru.khvsoft.WOM.entity.ContractWork;
import ru.khvsoft.WOM.entity.Employee;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface EmployeeService {
List<Employee> getAllEmployee();
Employee addEmployee(Employee employee);
Optional<Employee> getEmployee(long id);
void deleteEmployee(long id);
List<EmployeeWorkListDTO> getEmployeeWhereDataContractWork(LocalDate dateWork, long companyId);
ContractWork toContractWorkFromEmployeeWorkListDTO(EmployeeWorkListDTO employeeWorkListDTOS, LocalDate dateWork);
void toCopyContractWorkFromEmployeeWorkListDTO(LocalDate dateWork1, LocalDate dateWork2);
void deleteEmployeeWhereDataContractWork(LocalDate dateWork);
void setContractWorkCopyFromLastContractWork(LocalDate data, long company_id);
}
