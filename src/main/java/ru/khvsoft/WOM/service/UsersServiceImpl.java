package ru.khvsoft.WOM.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.khvsoft.WOM.entity.Users;
import ru.khvsoft.WOM.repo.UserRepo;
import ru.khvsoft.WOM.repo.UserRolesRepo;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class UsersServiceImpl implements UsersService {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    UserRepo userRepo;

    @Autowired
    UserRolesRepo userRolesRepo;

    @Override
    public boolean addUser(Users users) {
        if (isValidationUser(users)) {
            users.setPassword(passwordEncoder.encode(users.getPassword()));
            userRepo.save(users);
            return true;
        }
        log.debug("not addUser");
        return false;
    }

    @Override
    public boolean updateUser(Users users) {
        if (isValidationUpdateUser(users)) {
            users.setPassword(passwordEncoder.encode(users.getPassword()));
            userRepo.save(users);
            return true;
        }
        log.debug("not addUser");
        return false;
    }

    @Override
    public boolean isValidationUser(Users users) {
        if (isCorrectPassword(users)&&isCorrectEmail(users)&&isCorrectUserName(users)&&existUserInDatabase(users)&&!users.getUsersRoles().isEmpty()) {return true;}
        else {log.debug("not isValidationUser"); return false;}
    }


    public boolean isValidationUpdateUser(Users users) {
        if (isCorrectPassword(users)&&isCorrectEmail(users)&&isCorrectUserName(users)&&!users.getUsersRoles().isEmpty()) {return true;}
        else {log.debug("not isValidationUser"); return false;}
    }

    @Override
    public boolean isCorrectPassword(Users users) {
        if (users.getPassword()==null) {
            log.debug("null isCorrectPassword");
            return false;
        }
        if (!users.getPassword().matches("^[^\\s]{5,}$")) {
            log.debug("not isCorrectPassword");
            return false;
        }
        return true;
    }

    @Override
    public boolean isCorrectUserName(Users users) {
        if (users.getName()==null) {
            log.debug("null isCorrectUserName");
            return false;
        }

        if (!users.getName().matches("^[^\\s]{3,15}$")) {
            log.debug("not isCorrectUserName");
            return false;
        }
        return true;
    }

    @Override
    public boolean existUserInDatabase(Users users) {
        if (users.getName()==null) {
            log.debug("null existUserInDatabase");
            return false;
        }
        if (!userRepo.existUserInDatabase(users.getName()).isEmpty()) {
            log.debug("not existUserInDatabase");
            return false;
        }
        return true;
    }

    @Override
    public boolean isCorrectEmail(Users users) {
        if (users.getEmail()==null) {
            log.debug("null isCorrectEmail");
            return false;
        }

        if (!users.getEmail().matches("^([\\w]{1,})(@)([\\w]{1,})(\\.)(ru|com)$")) {
            log.debug("not isCorrectEmail");
            return false;
        }
        return true;
    }

    @Override
    public List<Users> getAllUsers() {
        return userRepo.findAll();
    }

    @Override
    public Optional<Users> getUserById(long id) {
        return userRepo.findById(id);
    }

    @Override
    public void deleteUser(long id) {
        userRepo.deleteById(id);
    }

    @Override
    public Users getUserByUserName(String userName) {
        return userRepo.getUserByUserName(userName);
    }
}
