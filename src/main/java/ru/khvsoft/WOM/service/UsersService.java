package ru.khvsoft.WOM.service;

import ru.khvsoft.WOM.classes.NameRole;
import ru.khvsoft.WOM.entity.Users;

import java.util.List;
import java.util.Optional;

public interface UsersService {
    boolean addUser(Users users);
    boolean updateUser(Users users);
    boolean isValidationUser(Users users);
    boolean isCorrectPassword(Users users);
    boolean isCorrectUserName(Users users);
    boolean existUserInDatabase(Users users);
    boolean isCorrectEmail(Users users);
    List<Users> getAllUsers();
    Optional<Users> getUserById(long id);
    void deleteUser(long id);
    Users getUserByUserName(String userName);
}
