package ru.khvsoft.WOM.service;

import ru.khvsoft.WOM.dto.Reports.WorkEmployeeReportsDTO;

import java.time.LocalDate;
import java.util.List;

public interface WorkEmployeeReportsDTOService {

    List<WorkEmployeeReportsDTO> getWorkEmployeeReportsDTO(LocalDate startDate, LocalDate endDate, long company_id);

}
