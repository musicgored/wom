package ru.khvsoft.WOM.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.khvsoft.WOM.entity.Contract;
import ru.khvsoft.WOM.repo.ContractRepo;

import java.util.List;
import java.util.Optional;

@Service
public class ContractServiceImpl implements ContractService {

    @Autowired
    ContractRepo contractRepo;

    @Override
    public Contract addContract(Contract contract) {
        return contractRepo.save(contract);
    }

    @Override
    public Optional<Contract> findContract(long id) {
        return contractRepo.findById(id);
    }

    @Override
    public void deleteContract(long id) {
        contractRepo.deleteById(id);
    }

    @Override
    public List<Contract> findAllContract() {
        return contractRepo.findAll();
    }
}
