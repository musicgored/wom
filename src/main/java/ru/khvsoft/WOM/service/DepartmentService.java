package ru.khvsoft.WOM.service;

import ru.khvsoft.WOM.entity.Department;

import java.util.List;
import java.util.Optional;

public interface DepartmentService {
Department addDepartment(Department department);
List<Department> getDepartment();
Optional<Department> findDepartmentById(long id);
void deleteDepartmentById(long id);
}
