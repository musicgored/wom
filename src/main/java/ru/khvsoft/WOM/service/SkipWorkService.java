package ru.khvsoft.WOM.service;

import ru.khvsoft.WOM.classes.ReasonSkip;
import ru.khvsoft.WOM.entity.SkipWork;

import java.util.List;
import java.util.Optional;

public interface SkipWorkService {
SkipWork addSkipWork (SkipWork skipWork);
List<SkipWork> finAllSkipWork();
Optional<SkipWork> findSkipWorkById(long id);
void deleteSkipWorkById(long id);
List<SkipWork> selectSkipWorkWhereReasonSkip(ReasonSkip reasonSkip);
List<SkipWork> selectSkipWorkWhereEmployeeIdAndReasonSkip(long id, ReasonSkip reasonSkip);
void addSkipWorkIFNotEmpty(SkipWork skipWork);
}
