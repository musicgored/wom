package ru.khvsoft.WOM.service;

import ru.khvsoft.WOM.entity.ContractWork;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface ContractWorkService {
ContractWork addContractWork(ContractWork contractWork);
Optional<ContractWork> findContractWorkById(long id);
List<ContractWork> findContractWorkAll();
void deleteContractWorkById(long id);
List<ContractWork> selectContactWorkWhereEmployeeId(long id);
List<ContractWork> selectContractWorkWhereEmployeeIdAndDateWork(long id, LocalDate dateWork);
List<ContractWork> selectContractWorkBetweenStartDateAndEndDate(LocalDate startDate, LocalDate endDate);
}
