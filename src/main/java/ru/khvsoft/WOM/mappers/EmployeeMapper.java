package ru.khvsoft.WOM.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import ru.khvsoft.WOM.dto.EmployeeDTO;
import ru.khvsoft.WOM.entity.Employee;

@Mapper (componentModel = "spring")
public interface EmployeeMapper {
    EmployeeDTO toEmployeeDTO(Employee employee);
    Employee toEmployee(EmployeeDTO employeeDTO);
}
