package ru.khvsoft.WOM.classes;


public enum ReasonSkip {

NOREASON("За свой счёт"),
MEDICAL("Больничный"),
VACATION("Отпуск"),
WEEKEND("Выходной");

private String value;

ReasonSkip(String value){
    this.value=value;
}

public String getValue(){return value;}

}
