package ru.khvsoft.WOM.configs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import javax.sql.DataSource;

@Configuration
public class SecurityConfig{

    @Autowired
    DataSource dataSource;



    @Configuration
    public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

        @Bean
        public PasswordEncoder getPasswordEncoder(){
            return new BCryptPasswordEncoder(8);
        }

        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth.jdbcAuthentication()
                    .dataSource(dataSource)
                    .usersByUsernameQuery("select u.name, u.password, u.active from users u where u.name=?")
                    .authoritiesByUsernameQuery("select u.name, ur.name_role from users u inner join roles_for_users r on (u.id=r.user_id) inner join users_role ur on (r.usersroles_id=ur.id) where u.name=?");

        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {

            http
                    .httpBasic()
                    .and()
                    .authorizeRequests()
                    .antMatchers(HttpMethod.GET, "/api/public/**").hasRole("USER")
                    .antMatchers(HttpMethod.POST, "/api/public/**").hasRole("USER")
                    .antMatchers(HttpMethod.PUT, "/api/public/**").hasRole("USER")
                    .antMatchers(HttpMethod.PATCH, "/api/public/**").hasRole("USER")
                    .antMatchers(HttpMethod.DELETE, "/api/public/**").hasRole("USER")
                    .antMatchers(HttpMethod.GET, "/api/service/**").hasRole("SUPER_ADMIN")
                    .antMatchers(HttpMethod.POST, "/api/service/**").hasRole("SUPER_ADMIN")
                    .antMatchers(HttpMethod.PUT, "/api/service/**").hasRole("SUPER_ADMIN")
                    .antMatchers(HttpMethod.PATCH, "/api/service/**").hasRole("SUPER_ADMIN")
                    .antMatchers(HttpMethod.DELETE, "/api/service/**").hasRole("SUPER_ADMIN")
                    .and()
                    .csrf().disable()
                    .formLogin().disable();
        }


    }
}
