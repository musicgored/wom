package ru.khvsoft.WOM.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "users")
public class Users {
@Id
@GeneratedValue(strategy = GenerationType.AUTO)
@Column(name = "id")
private long id;

@Column(name = "name")
private String name;

@Column(name = "password")
private String password;

@Column(name = "active")
private boolean active;

@Column(name= "email")
private String email;

@ManyToMany(fetch = FetchType.EAGER,cascade = {CascadeType.ALL})
@JoinTable(name = "roles_for_users", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "usersroles_id"))
Set<UsersRole> usersRoles = new HashSet<>();

@JsonIgnore
@ManyToMany(fetch = FetchType.LAZY,cascade = {CascadeType.ALL})
@JoinTable(name = "users_company", joinColumns = @JoinColumn(name = "users_id"), inverseJoinColumns = @JoinColumn(name = "company_id"))
Set<Company> companyList = new HashSet<>();

@JoinColumn(name = "selected_company")
@ManyToOne (cascade = CascadeType.ALL)
Company selectedCompany;



    public Users(String name, String password, Set<UsersRole> usersRoles) {
        this.name = name;
        this.password = password;
        this.usersRoles = usersRoles;
    }

    public Users(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public Users(long id, String name, String password) {
        this.id = id;
        this.name = name;
        this.password = password;
    }

    public Users(String name, String password, Company selectedCompany) {
        this.name = name;
        this.password = password;
        this.selectedCompany = selectedCompany;
    }

    public void addRole(UsersRole usersRole){
        this.usersRoles.add(usersRole);
}



}
