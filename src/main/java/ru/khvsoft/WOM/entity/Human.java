package ru.khvsoft.WOM.entity;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Data
@NoArgsConstructor
public class Human {

@Id
@GeneratedValue(strategy = GenerationType.AUTO)
private long id;
private String name;
    public Human(String name) {
        this.name = name;
    }
}
