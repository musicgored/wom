package ru.khvsoft.WOM.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@NoArgsConstructor
@Data
public class Woman extends Human{

@Id
@GeneratedValue(strategy = GenerationType.AUTO)
private long id;

private String nameWoman;

    public Woman(String nameWoman, String name) {
        super(name);
        this.nameWoman = nameWoman;
    }

}
