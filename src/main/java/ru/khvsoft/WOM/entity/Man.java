package ru.khvsoft.WOM.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@Entity
public class Man extends Human{

@Id
@GeneratedValue(strategy = GenerationType.AUTO)
private long id;

private String nameMan;

    public Man(String nameMan, String name) {
        super(name);
        this.nameMan = nameMan;
    }

    @Override
    public String toString() {
        return "Man{" +
                "id=" + id +
                ", nameMan='" + nameMan + '\'' +
                ", nameMan='" + super.getName() + '\'' +
                '}';
    }
}


