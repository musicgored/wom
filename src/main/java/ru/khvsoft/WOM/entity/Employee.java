package ru.khvsoft.WOM.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.khvsoft.WOM.utils.EntityIdResolver;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Objects;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "employee")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        scope = Employee.class,
        resolver = EntityIdResolver.class,
        property = "id"
)
public class Employee {

    public Employee(@NotBlank(message = "Поле Имя не должно быть пустым") String name, @NotBlank(message = "Поле Отчество не должно быть пустым") String lastName, @NotBlank(message = "Поле Фамилия не должно быть пустым") String middleName) {
        this.name = name;
        this.lastName = lastName;
        this.middleName = middleName;
    }

    public Employee(long id, @NotBlank(message = "Поле Имя не должно быть пустым") String name, @NotBlank(message = "Поле Отчество не должно быть пустым") String lastName, @NotBlank(message = "Поле Фамилия не должно быть пустым") String middleName) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.middleName = middleName;
    }

@Id
@GeneratedValue(strategy = GenerationType.AUTO)
@Column(name = "id")
private long id;

@NotBlank(message = "Поле Имя не должно быть пустым")
@Column(name = "name")
private String name;

@NotBlank(message = "Поле Отчество не должно быть пустым")
@Column(name = "last_name")
private String lastName;

@NotBlank(message = "Поле Фамилия не должно быть пустым")
@Column(name = "middle_name")
private String middleName;

@ManyToOne(cascade = CascadeType.REFRESH)
@JoinColumn(name = "department_id")
@JsonIdentityReference(alwaysAsId = true)
Department department;

@ManyToOne(cascade = {CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH})
@JoinColumn(name = "workschedule_id")
@JsonIdentityReference(alwaysAsId = true)
WorkSchedule workSchedule;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return id == employee.id &&
                Objects.equals(name, employee.name) &&
                Objects.equals(lastName, employee.lastName) &&
                Objects.equals(middleName, employee.middleName) &&
                Objects.equals(department, employee.department);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, lastName, middleName, department);
    }
}


