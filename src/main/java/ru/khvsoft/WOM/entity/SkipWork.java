package ru.khvsoft.WOM.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.khvsoft.WOM.classes.ReasonSkip;
import ru.khvsoft.WOM.utils.EntityIdResolver;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "skip_work")@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        scope = SkipWork.class,
        resolver = EntityIdResolver.class,
        property = "id"
)

@Data
@NoArgsConstructor
public class SkipWork {
@Id
@GeneratedValue(strategy = GenerationType.AUTO)
private long id;


@Column(name = "reason")
@Enumerated(EnumType.STRING)
@JsonIdentityReference(alwaysAsId = true)
private ReasonSkip reasonSkip;


@ManyToOne(cascade = CascadeType.REFRESH)
@JoinColumn(name = "employee_id")
@JsonIdentityReference(alwaysAsId = true)
Employee employee;


@Column(name = "start_date", columnDefinition = "DATE")
private LocalDate startDate;


@Column(name = "end_date", columnDefinition = "DATE")
private LocalDate endDate;


    public SkipWork(long id, ReasonSkip reasonSkip, LocalDate startDate, LocalDate endDate) {
        this.id = id;
        this.reasonSkip = reasonSkip;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public SkipWork(ReasonSkip reasonSkip, LocalDate startDate, LocalDate endDate) {
        this.reasonSkip = reasonSkip;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public SkipWork(ReasonSkip reasonSkip, Employee employee, LocalDate startDate, LocalDate endDate) {
        this.reasonSkip = reasonSkip;
        this.employee = employee;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public SkipWork(long id, ReasonSkip reasonSkip, Employee employee, LocalDate startDate, LocalDate endDate) {
        this.id = id;
        this.reasonSkip = reasonSkip;
        this.employee = employee;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SkipWork skipWork = (SkipWork) o;
        return reasonSkip == skipWork.reasonSkip &&
                Objects.equals(employee, skipWork.employee) &&
                Objects.equals(startDate, skipWork.startDate) &&
                Objects.equals(endDate, skipWork.endDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, reasonSkip, employee, startDate, endDate);
    }
}
