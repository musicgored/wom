package ru.khvsoft.WOM.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.khvsoft.WOM.utils.EntityIdResolver;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@NoArgsConstructor
@Data
@AllArgsConstructor
@Table(name = "contract")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        scope = Contract.class,
        resolver = EntityIdResolver.class,
        property = "id"
)

public class Contract {

@Id
@GeneratedValue(strategy = GenerationType.AUTO)
private long id;

@Column(name = "name")
private String name;

@Column(name = "numbre")
private String numbre;

@Column(name = "color")
private String color;

@Column(name = "start_date",columnDefinition = "DATE")
private LocalDate start_date;

@Column(name = "end_date",columnDefinition = "DATE")
private LocalDate end_date;

@ManyToOne
@JoinColumn(name = "company_id")
@JsonIdentityReference(alwaysAsId = true)
private Company company;

@ManyToOne
@JoinColumn(name = "type_contract_id")
@JsonIdentityReference(alwaysAsId = true)
private TypeContract typeContract;

    public Contract(String name, LocalDate start_date, LocalDate end_date) {
        this.name = name;
        this.start_date = start_date;
        this.end_date = end_date;
    }

    public Contract(long id, String name, LocalDate start_date, LocalDate end_date) {
        this.id = id;
        this.name = name;
        this.start_date = start_date;
        this.end_date = end_date;
    }

    public Contract(String name, LocalDate start_date, LocalDate end_date, TypeContract typeContract) {
        this.name = name;
        this.start_date = start_date;
        this.end_date = end_date;
        this.typeContract = typeContract;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contract contract = (Contract) o;
        return id == contract.id &&
                Objects.equals(name, contract.name) &&
                Objects.equals(start_date, contract.start_date) &&
                Objects.equals(end_date, contract.end_date) &&
                Objects.equals(typeContract, contract.typeContract);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, start_date, end_date, typeContract);
    }


}


