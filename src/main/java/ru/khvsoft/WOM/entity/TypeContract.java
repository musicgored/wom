package ru.khvsoft.WOM.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.khvsoft.WOM.utils.EntityIdResolver;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Data
@NoArgsConstructor
@Table(name = "type_contract")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        scope = TypeContract.class,
        resolver = EntityIdResolver.class,
        property = "id"
)
public class TypeContract {
@Id
@GeneratedValue(strategy = GenerationType.AUTO)
private long id;

@Column(name = "type_name")
private String typeName;

@Column(name = "period", columnDefinition = "DATE")
private LocalDate period;

@Column(name = "period_end", columnDefinition = "DATE")
private LocalDate period_end;


@JoinColumn(name = "company_id")
@ManyToOne(cascade = CascadeType.ALL)
private Company company;

    public TypeContract(long id) {
        this.id = id;
    }

    public TypeContract(String typeName, LocalDate period) {
        this.typeName = typeName;
        this.period = period;
    }

    public TypeContract(long id, String typeName, LocalDate period) {
        this.id = id;
        this.typeName = typeName;
        this.period = period;
    }

    public TypeContract(String typeName, LocalDate period, Company company) {
        this.typeName = typeName;
        this.period = period;
        this.company = company;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TypeContract that = (TypeContract) o;
        return id == that.id &&
                Objects.equals(typeName, that.typeName) &&
                Objects.equals(period, that.period);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, typeName, period);
    }
}
