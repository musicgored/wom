package ru.khvsoft.WOM.entity;

import com.fasterxml.jackson.annotation.JsonIdentityReference;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@NoArgsConstructor
@Data
@Table(name = "contract_work")
public class ContractWork {
@Id
@GeneratedValue(strategy = GenerationType.AUTO)
private long id;

@Column(name = "date_word", columnDefinition = "DATE")
private LocalDate dateWork;

@ManyToOne(cascade = CascadeType.REFRESH)
@JoinColumn(name = "employee_id")
@JsonIdentityReference(alwaysAsId = true)
private Employee employee;

@ManyToOne(cascade = CascadeType.REFRESH)
@JoinColumn(name = "contract_id")
@JsonIdentityReference(alwaysAsId = true)
private Contract contract;

    public ContractWork(LocalDate dateWork, Employee employee, Contract contract) {
        this.dateWork = dateWork;
        this.employee = employee;
        this.contract = contract;
    }

    public ContractWork(LocalDate dateWork) {
        this.dateWork = dateWork;
    }

    public ContractWork(long id, LocalDate dateWork) {
        this.id = id;
        this.dateWork = dateWork;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContractWork that = (ContractWork) o;
        return id == that.id &&
                Objects.equals(dateWork, that.dateWork) &&
                Objects.equals(employee, that.employee) &&
                Objects.equals(contract, that.contract);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, dateWork, employee, contract);
    }
}
