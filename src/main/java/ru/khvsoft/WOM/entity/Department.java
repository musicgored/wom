package ru.khvsoft.WOM.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.khvsoft.WOM.utils.EntityIdResolver;

import javax.persistence.*;
import java.util.Objects;

@Table(name = "department")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        scope = Department.class,
        resolver = EntityIdResolver.class,
        property = "id"
)
@Entity
@Data
@NoArgsConstructor
public class Department {
@Column(name = "id")
@Id
@GeneratedValue(strategy = GenerationType.AUTO)
private long id;

@Column(name = "name_department")
private String nameDepartment;

@ManyToOne(cascade = CascadeType.REFRESH)
@JoinColumn(name = "company_id")
private Company company;


    public Department(String nameDepartment) {
        this.nameDepartment = nameDepartment;
    }

    public Department(long id, String nameDepartment) {
        this.id = id;
        this.nameDepartment = nameDepartment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return id == that.id &&
                Objects.equals(nameDepartment, that.nameDepartment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nameDepartment);
    }
}
