package ru.khvsoft.WOM.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.khvsoft.WOM.classes.NameRole;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "users_role")
@JsonIgnoreProperties("usersList")
public class UsersRole {

@Id
@GeneratedValue(strategy = GenerationType.AUTO)
@Column(name = "id")
private long id;

@Column(name = "name_role")
@Enumerated(EnumType.STRING)
private NameRole nameRole;

@ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH})
@JoinTable(name = "roles_for_users", joinColumns = @JoinColumn(name = "usersroles_id"), inverseJoinColumns = @JoinColumn(name = "user_id"))
List<Users> usersList = new ArrayList<>();

    public UsersRole(NameRole nameRole) {
        this.nameRole = nameRole;
    }
}


