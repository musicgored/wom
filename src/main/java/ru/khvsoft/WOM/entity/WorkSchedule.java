package ru.khvsoft.WOM.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.khvsoft.WOM.utils.EntityIdResolver;

import javax.persistence.*;

@Entity
@Table(name = "work_shedule")
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        scope = WorkSchedule.class,
        resolver = EntityIdResolver.class,
        property = "id"
)
@NoArgsConstructor
@Data
public class WorkSchedule {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "quantity_work")
    private int quantity_work;

    @Column(name = "quantity_weekend")
    private int quantity_weekend;

    @Column(name = "work_weekend")
    private boolean work_weekend;

    @ManyToOne(cascade = {CascadeType.MERGE,CascadeType.PERSIST,CascadeType.REFRESH})
    @JoinColumn(name = "company_id")
    Company company;

}
