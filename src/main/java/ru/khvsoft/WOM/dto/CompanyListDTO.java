package ru.khvsoft.WOM.dto;
import lombok.Data;
import ru.khvsoft.WOM.entity.Company;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
public class CompanyListDTO {
private long id;
private String value;

    public CompanyListDTO(Company company) {
        this.id =  company.getId();
        this.value = company.getName();
    }

    public static List<CompanyListDTO> getCompanyListDTO(List<Company> companyList){
        List <CompanyListDTO> companyListDTO = new ArrayList<>();
        for (Company company:companyList) {
            companyListDTO.add(new CompanyListDTO(company));
        }
        return companyListDTO;
    }

}
