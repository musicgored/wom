package ru.khvsoft.WOM.dto;

import lombok.Data;
import ru.khvsoft.WOM.entity.WorkSchedule;

import java.util.ArrayList;
import java.util.List;

@Data
public class WorkScheduleListDTO {
private long id;
private String value;

    public WorkScheduleListDTO(WorkSchedule workSchedule) {
        this.id = workSchedule.getId();
        this.value = workSchedule.getName();
    }

    public static List<WorkScheduleListDTO> toWorkScheduleListDTOList(List<WorkSchedule> workScheduleList){
        List <WorkScheduleListDTO> workScheduleListDTO = new ArrayList<>();
        for (WorkSchedule wh:workScheduleList) {
            workScheduleListDTO.add(new WorkScheduleListDTO(wh));
        }
        return workScheduleListDTO;
    }
}
