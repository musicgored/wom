package ru.khvsoft.WOM.dto;

import com.fasterxml.jackson.annotation.JsonIdentityReference;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.khvsoft.WOM.classes.ReasonSkip;
import ru.khvsoft.WOM.entity.Contract;
import ru.khvsoft.WOM.entity.Employee;
import ru.khvsoft.WOM.entity.SkipWork;

@Data
@NoArgsConstructor
public class EmployeeWorkListDTO {
@JsonIdentityReference(alwaysAsId = true)
private Employee employee;
@JsonIdentityReference(alwaysAsId = true)
private Contract contract;
@JsonIdentityReference(alwaysAsId = true)
private ReasonSkip reasonSkip;


    public EmployeeWorkListDTO(Employee employee) {
        this.employee = employee;
    }

}
