package ru.khvsoft.WOM.dto;

import lombok.Data;
import ru.khvsoft.WOM.classes.ReasonSkip;

import java.util.ArrayList;
import java.util.List;

@Data
public class ReasonSkipListDTO {
private String id;
private String value;

    public ReasonSkipListDTO(ReasonSkip reasonSkip) {
        this.id = reasonSkip.name();
        this.value = reasonSkip.getValue();
    }

    public static List<ReasonSkipListDTO> toReasonSkipListDTO(){
        List<ReasonSkipListDTO> reasonSkipListDTOList = new ArrayList<>();
        for (ReasonSkip r:ReasonSkip.values()) {
            reasonSkipListDTOList.add(new ReasonSkipListDTO(r));
        }
        return reasonSkipListDTOList;
    }
}
