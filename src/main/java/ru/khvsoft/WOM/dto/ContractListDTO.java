package ru.khvsoft.WOM.dto;

import lombok.Data;
import ru.khvsoft.WOM.entity.Contract;

import java.util.ArrayList;
import java.util.List;

@Data
public class ContractListDTO {
private long id;
private String value;

    public ContractListDTO(Contract contract) {
        this.id = contract.getId();
        if (!contract.getColor().isEmpty()) {
            this.value= contract.getName()+"#"+contract.getColor();
        } else {
        this.value = contract.getName();}
    }

    public static List<ContractListDTO> toContractListDTO(List<Contract> contractList){
       List <ContractListDTO> contractListDTO = new ArrayList<>();
        for (Contract c:contractList) {
            contractListDTO.add(new ContractListDTO(c));
        }
        return contractListDTO;
    }
}
