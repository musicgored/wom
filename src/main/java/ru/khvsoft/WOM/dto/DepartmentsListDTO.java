package ru.khvsoft.WOM.dto;

import lombok.Data;
import ru.khvsoft.WOM.entity.Department;

import java.util.ArrayList;
import java.util.List;

@Data
public class DepartmentsListDTO {
private long id;
private String value;

    public DepartmentsListDTO(Department department) {
        this.id = department.getId();
        this.value = department.getNameDepartment();
    }

    public static List<DepartmentsListDTO> getDepartmentListDTO(List<Department> departmentList){
        List <DepartmentsListDTO> departmentsListDTO =  new ArrayList<>();
        for (Department d:departmentList) {
            departmentsListDTO.add(new DepartmentsListDTO(d));
        }
        return departmentsListDTO;
    }
}
