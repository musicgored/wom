package ru.khvsoft.WOM.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import ru.khvsoft.WOM.entity.TypeContract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
public class TypeContractListItemDTO {
private Long id;
private String value;

public static List<TypeContractListItemDTO> getTypeContractListItemDTO (List<TypeContract> typeContractList) {
    List<TypeContractListItemDTO> typeContractListItemDTOList = new ArrayList<>();
    for (TypeContract typeContract:typeContractList) {
        typeContractListItemDTOList.add(new TypeContractListItemDTO(typeContract.getId(),typeContract.getTypeName()));
    }
    return typeContractListItemDTOList;
}



}
