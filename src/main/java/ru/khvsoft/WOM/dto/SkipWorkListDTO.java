package ru.khvsoft.WOM.dto;

import lombok.Data;
import ru.khvsoft.WOM.entity.SkipWork;

import java.util.ArrayList;
import java.util.List;

@Data
public class SkipWorkListDTO {
private long id;
private String value;

    public SkipWorkListDTO(SkipWork skipWork) {
        this.id = skipWork.getId();
        this.value = skipWork.getReasonSkip().getValue();
    }

    public static List<SkipWorkListDTO> toSkipWorkListDTO(List<SkipWork> skipWorkList){
        List<SkipWorkListDTO> skipWorkListDTOS = new ArrayList<>();
        for (SkipWork sk: skipWorkList) {
            skipWorkListDTOS.add(new SkipWorkListDTO(sk));
        }
        return skipWorkListDTOS;
    }
}
