package ru.khvsoft.WOM.dto;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import ru.khvsoft.WOM.entity.Company;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@Data
public class UserInfoDTO {
private String userName;
private Collection<GrantedAuthority> accessGroup;

@JsonIdentityReference(alwaysAsId = true)
private List<CompanyListDTO> company;


    public UserInfoDTO(User user, List<CompanyListDTO> company) {
        this.userName = user.getUsername();
        this.accessGroup = user.getAuthorities();
        this.company = company;
    }
}
