package ru.khvsoft.WOM.dto;

import lombok.Data;

@Data
public class EmployeeDTO {
    private long id;
    private String name;
    private String lastName;
    private String middleName;
}
