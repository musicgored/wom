package ru.khvsoft.WOM.dto.Reports;

import com.fasterxml.jackson.annotation.JsonIdentityReference;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.khvsoft.WOM.entity.Employee;

@NoArgsConstructor
@Data
public class WorkEmployeeReportsDTO {
    @JsonIdentityReference(alwaysAsId = true)
private Employee employee;
private long countWork;
private long countMedical;
private long countVacation;
private long countWeekend;
private long noReason;
}
