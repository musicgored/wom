package ru.khvsoft.WOM.dto;

import lombok.Data;
import ru.khvsoft.WOM.entity.Employee;

import java.util.ArrayList;
import java.util.List;

@Data
public class EmployeeListDTO {
private long id;
private String value;

    public EmployeeListDTO(Employee employee) {
        this.id = employee.getId();
        this.value = employee.getLastName();
    }

    public static List<EmployeeListDTO> toEmployeeListDTO(List<Employee> employeeList){
        List<EmployeeListDTO> employeeListDTOS = new ArrayList<>();
        for (Employee e:employeeList) {
            employeeListDTOS.add(new EmployeeListDTO(e));
        }
        return employeeListDTOS;
    }
}
