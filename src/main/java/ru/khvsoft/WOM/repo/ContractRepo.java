package ru.khvsoft.WOM.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.khvsoft.WOM.entity.Contract;
import ru.khvsoft.WOM.entity.TypeContract;

import java.util.List;

public interface ContractRepo extends JpaRepository<Contract, Long> {
    @Query("select c.typeContract from Contract c inner join c.company com where com.id=:companyId")
    List<TypeContract> getTypeContractWhereCompanyId(@Param("companyId") long companyId);

    @Query("select c from Contract c where c.company.id=:companyId")
    List<Contract> getContractWhereCompanyId(@Param("companyId") long companyId);
}
