package ru.khvsoft.WOM.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.khvsoft.WOM.entity.ContractWork;

import java.time.LocalDate;
import java.util.List;

public interface ContractWorkRepo extends JpaRepository<ContractWork,Long> {

    @Query("select cw from ContractWork cw inner join cw.employee e where e.id=:id")
    List<ContractWork> selectContactWorkWhereEmployeeId(@Param("id") long id);

    @Query("select cw from ContractWork cw inner join cw.employee e where e.id=:id and cw.dateWork=:dateWork")
    List<ContractWork> selectContractWorkWhereEmployeeIdAndDateWork(@Param("id") long id, @Param("dateWork") LocalDate dateWork);

    @Query("select cw from ContractWork cw where cw.dateWork=:dateWork")
    List<ContractWork> selectContractWorkWhereDateWork(@Param("dateWork") LocalDate dateWork);

    @Query("select cw from ContractWork cw where cw.dateWork>=:startDate and cw.dateWork<=:endDate")
    List<ContractWork> selectContractWorkBetweenStartDateAndEndDate(@Param("startDate") LocalDate startDate, @Param("endDate") LocalDate endDate);

    @Query("select cw from ContractWork cw inner join cw.contract c where c.company.id=:companyId")
    List<ContractWork> selectContractWordWhereCompanyId(@Param("companyId") long companyId);

    @Modifying
    @Query("delete from ContractWork cw where cw.dateWork=:workDate")
    void deleteEmployeeWhereDataContractWork(@Param("workDate") LocalDate workDate);

    @Query("select cw from ContractWork cw inner join cw.employee e where e.id=:employee_id and cw.dateWork=:date")
    List<ContractWork> selectContractWorkWhereDateAndEmployee(@Param("date") LocalDate date, @Param("employee_id") long employee_id);

    @Query("select cw from ContractWork  cw inner join cw.employee e where e.id=:employee_id and (cw.dateWork between :startDate and :endDate)")
    List<ContractWork> selectContractWorkWhereDateWorkAndEmployee(@Param("employee_id") long employee_id,@Param("startDate") LocalDate startDate, @Param("endDate") LocalDate endDate);

}
