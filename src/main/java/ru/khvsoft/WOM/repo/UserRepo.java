package ru.khvsoft.WOM.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.khvsoft.WOM.entity.Users;

import java.util.List;

public interface UserRepo extends JpaRepository<Users, Long> {
    @Query("select u from Users u where u.name=:name")
    List<Users> existUserInDatabase (@Param("name") String name);

    @Query("select u from Users u where u.name=:name")
    Users getUserByUserName(@Param("name") String name);
}
