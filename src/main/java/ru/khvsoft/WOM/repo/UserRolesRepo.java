package ru.khvsoft.WOM.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.khvsoft.WOM.entity.UsersRole;

public interface UserRolesRepo extends JpaRepository<UsersRole,Long> {
}
