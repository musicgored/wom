package ru.khvsoft.WOM.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.khvsoft.WOM.entity.Company;

import java.util.List;

@Repository
public interface CompanyRepo extends JpaRepository<Company,Long> {
    @Query ("select c from Company c inner join c.usersList u where u.id=:id")
    List<Company> getCompanyWhereUserId(@Param("id") long id);
}
