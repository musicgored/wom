package ru.khvsoft.WOM.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.khvsoft.WOM.entity.WorkSchedule;

@Repository
public interface WorkScheduleRepo extends JpaRepository<WorkSchedule,Long> {
}
