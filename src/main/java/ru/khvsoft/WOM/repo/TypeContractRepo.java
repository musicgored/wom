package ru.khvsoft.WOM.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Repository;
import ru.khvsoft.WOM.entity.TypeContract;

import java.util.List;

@Repository
public interface TypeContractRepo extends JpaRepository<TypeContract,Long> {

    @Query("select tc from TypeContract tc inner join tc.company c inner join c.usersList u where u.name=:userName")
    List<TypeContract> getTypeContractWhereCompanySelected(@Param("userName") String userName);

}
