package ru.khvsoft.WOM.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.khvsoft.WOM.entity.Woman;

@Repository
public interface WomanRepo extends JpaRepository<Woman,Integer> {
}
