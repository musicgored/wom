package ru.khvsoft.WOM.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.khvsoft.WOM.entity.Department;

import java.util.List;

public interface DepartmentRepo extends JpaRepository<Department, Long> {
    @Query("select d from Department d where d.company.id=:companyId")
    List<Department> getDepartmentWhereCompanyId(@Param("companyId") long companyId);
}
