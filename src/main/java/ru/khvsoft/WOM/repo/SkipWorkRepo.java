package ru.khvsoft.WOM.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.khvsoft.WOM.classes.ReasonSkip;
import ru.khvsoft.WOM.entity.Employee;
import ru.khvsoft.WOM.entity.SkipWork;

import java.time.LocalDate;
import java.util.List;

public interface SkipWorkRepo extends JpaRepository<SkipWork,Long> {

    @Query ("select sw from SkipWork sw where sw.reasonSkip=:reasonSkip")
    List<SkipWork> selectSkipWorkWhereReasonSkip(@Param("reasonSkip") ReasonSkip reasonSkip);

    @Query ("select sw from SkipWork sw inner join sw.employee e where e.id=:id and sw.reasonSkip=:reasonSkip")
    List<SkipWork> selectSkipWorkWhereEmployeeIdAndReasonSkip(@Param("id") long id, @Param("reasonSkip") ReasonSkip reasonSkip);

    @Query("select sw from SkipWork sw inner join sw.employee e inner join e.department d inner join d.company c where c.id=:companyId")
    List<SkipWork> selectSkipWorkWhereCompanyId(@Param("companyId") long companyId);

    @Query("select sw from SkipWork sw where sw.startDate<=:skipDate and sw.endDate>=:skipDate")
    List<SkipWork> selectSkipWorkWhereSkipDate(@Param("skipDate") LocalDate skipDate);

    @Query("select sw from SkipWork sw inner join sw.employee e  where  (sw.startDate<=:startDate or sw.endDate>=:endDate) and e.id=:employee_id")
    List<SkipWork> selectSkipWorkWhereSkipDateWhereEmployeeId(@Param("startDate") LocalDate startDate,@Param("endDate") LocalDate endDate, @Param("employee_id") long employee_id);

    @Query("select sw from SkipWork sw inner join sw.employee e where e.id=:employee_id and :date between sw.startDate and sw.endDate")
    List<SkipWork> selectSkipWorkWhereDateAndEmployee(@Param("date") LocalDate date, @Param("employee_id") long employee_id);

    @Modifying
    @Query("delete from SkipWork sw where (:date between sw.startDate and sw.endDate)")
    void deleteSkipWorkWhereDateAndCompanyId(@Param("date") LocalDate date);

    @Query("select sw from SkipWork sw inner join sw.employee e where e.id=:employee_id and :startDate between sw.startDate and sw.endDate")
    List<SkipWork> findSkipWorkWhereEmployeeAndSkipWorkDate(@Param("employee_id") long employee_id,@Param("startDate") LocalDate startDate);
}
