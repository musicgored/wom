package ru.khvsoft.WOM.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.khvsoft.WOM.entity.Man;

@Repository
public interface ManRepo extends JpaRepository<Man,Integer> {
}
