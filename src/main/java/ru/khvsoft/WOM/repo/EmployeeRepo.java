package ru.khvsoft.WOM.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.khvsoft.WOM.entity.Employee;

import java.util.List;

public interface EmployeeRepo extends JpaRepository<Employee,Long> {
    @Query("select e from Employee e inner join e.department d inner join d.company c where c.id=:companyId")
    List<Employee> getEmployeeWhereCompanyId(@Param("companyId") long companyId);
}
