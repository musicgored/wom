package ru.khvsoft.WOM;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import ru.khvsoft.WOM.entity.Company;
import ru.khvsoft.WOM.entity.Contract;
import ru.khvsoft.WOM.entity.Users;
import ru.khvsoft.WOM.repo.CompanyRepo;
import ru.khvsoft.WOM.repo.ContractRepo;
import ru.khvsoft.WOM.repo.DepartmentRepo;
import ru.khvsoft.WOM.repo.UserRepo;
import ru.khvsoft.WOM.service.EmployeeService;
import ru.khvsoft.WOM.service.UsersService;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.Optional;

@Slf4j
@SpringBootApplication
public class WomApplication extends SpringBootServletInitializer implements CommandLineRunner {



	public static void main(String[] args) {
		SpringApplication.run(WomApplication.class, args);
	}


	@Transactional
	@Override
	public void run(String... args) throws Exception {

	}

}
