package ru.khvsoft.WOM.exeption_handling;

public class ControllerException extends RuntimeException{
    public ControllerException(String message) {
        super(message);
    }
}
